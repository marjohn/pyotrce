
import java.lang.System;
import java.io.*;

public class search {

    generator gen;
    board brd;

    //-----Hash Elements
    final int hashEXACT = 0;
    final int hashALPHA = 1;
    final int hashBETA = 2;
    final double valUNKNOWN = 2000;
//-----Hash Elements

    boolean flagMateFound = false;

//******************************************************
//*  Type : public                                     *
//*  Args :                                            *
//*  Aim  : The constructor of the class.              *
//******************************************************
//*         -=     Start of search            =-       *
//******************************************************
    public search(generator gen, board brd) throws IOException {
        this.gen = gen;
        this.brd = brd;
    }
//******************************************************
//*         -=     End   of search            =-       *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : This method is invoked whenever the prog   *
//*         must start thinking.First creates all      *
//*         pseudolegal moves by calling pseudo_legal()*
//*         method and then chooses the best move      *
//*         via the evaluate() method.                 *
//******************************************************
//*         -=    Start of search         =-           *
//******************************************************
    void search() throws IOException {

        //...Initialize the necessary variables
        gen.maxTime = gen.tmControl.TimeCutoff(gen.TimeControl_AllMoves, gen.TimeControl_XMoves,
                gen.TimeControl_XMoves_Number, gen.maxTime);
        gen.NODES = 0;

        //Set the start time of the search
        gen.startTime = gen.tmControl.GetTime();

        flagMateFound = false;
        gen.flagI_am_Mated_W = false;
        gen.flagI_am_Mated_B = false;
        gen.flagStalemated = false;
        //...Initialize the necessary variables

        //....Start of Iteration deepening
        for (int depth = 1;; depth++) {
            gen.DEPTH = depth;

            double val = AplhaBeta(depth, gen.TURN, gen.OTURN, -1000, 1000);
            System.out.println("Search :: " + depth + "  " + gen.BEST_MOVE);
            if (flagMateFound || gen.flagStalemated) {
                break;
            }
            if (gen.tmControl.TimeOut(gen.maxTime, gen.startTime) || gen.DEPTH == 5) {
                break;
            }
        }
        //....End of  Iteration deepening

//Calculating the Nodes and NPS
        long now = gen.tmControl.GetTime() - gen.startTime;
        if (now == 0) {
            now = 1;
        }
        long NPS = gen.NODES / now;
        System.out.println(" Calculating the Nodes and NPS");
        System.out.println(" -----------------------------");
        System.out.println(" Total Nodes  : " + gen.NODES);
        System.out.println(" Nodes per sec: " + NPS);
        System.out.println(" Thinking time: " + now + " seconds");
//Calculate the Nodes and NPS

    }
//******************************************************
//*         -=     End of search          =-           *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : This method is invoked whenever the prog   *
//*         must start thinking.First creates all      *
//*         pseudolegal moves by calling pseudo_legal()*
//*         method and then chooses the best move      *
//*         via the evaluate() method.                 *
//******************************************************
//*         -=     Start of AplhaBeta     =-           *
//******************************************************
    double AplhaBeta(int depth, int side, int oside, double alpha, double beta) throws IOException {

        int[][] temp_movelist = new int[100][4];

        int hashFlag = hashALPHA;
        int ply = gen.DEPTH - depth + 1;

        double[] temp_eval;
        temp_eval = new double[1000];

        int last = 0;
        String move = "", fr, t;

        gen.BEST_MOVE_INDEX = 0;

        gen.SEARCH_PLY++;

        //.........sEARCH THE HASH Table for the current position
        if ((gen.val = gen.SearchHash(depth, alpha, beta, side)) != valUNKNOWN) {
            //System.out.println("Alpha Beta:: Position found at HashTable,returned value="+ gen.val );
            return gen.val;
        }

        if (depth <= 0) {
            gen.val = gen.EVAL.Evaluate(gen.WHO_PLAY, gen.move_index);
            //......Record the current position to Hash Table
            // if(  (ply-1)<gen.DEPTH  )
            gen.RecordHash(depth, gen.val, hashEXACT, side, gen.PV[depth + 1]);
//System.out.println("Alpha Beta:: Just return the value=" + gen.val);
            return gen.val;
        }
//    System.out.println("Move list at depth " + depth);
//    movelist_print();

        //....Generate all Legal moves and store them at the temporary movelist
        if (depth == gen.DEPTH) {
            gen.SEARCH_PLY = 1;
        }
        gen.pseudo_legal(oside, side);

        last = gen.LAST;
//System.out.println("Alpha Beta:: LAST" + gen.LAST);
        //Adding NODES
        if (depth == 1) {
            gen.NODES += last;
        }

        //If NO moves are available check if a mate found or a stalemate
        if (last == 0) {
            gen.val = printResult(depth);
            alpha = gen.val;
            return gen.val;
        }

        //Copy the movelist array into a temp variable
        //System.arraycopy(brd.movelist,0,temp_movelist,0,last-1);
        for (int k = 0; k <= (last - 1); k++) {

            temp_movelist[k][0] = brd.movelist[k][0];
            temp_movelist[k][1] = brd.movelist[k][1];
            temp_movelist[k][2] = brd.movelist[k][2];

        }

        // Move ordering
        if (depth == gen.DEPTH) {
            move_ordering(depth, last, temp_movelist);
        }
        //....Generate all Legal moves and store them at the temporary movelist

        //....While there are moves on the temporary movelist
        for (int k = 0; k <= (last - 1); k++) {

            fr = brd.board_squares[brd.board_main[temp_movelist[k][0]]];
            t = brd.board_squares[brd.board_main[temp_movelist[k][1]]];
            move = fr + t;

            //Make move
            gen.make_move(brd.board_main[temp_movelist[k][0]], brd.board_main[temp_movelist[k][1]], move, temp_movelist[k][2]);

            // if(depth == gen.DEPTH)
            //System.out.println("Alpha Beta:: " + move);
            gen.PV[depth] = move;
            gen.BEST_MOVE = gen.PV[gen.DEPTH];
            //System.out.println("Alpha Beta:: " + ply + " ply searching move " + move + "    " +gen.eval[k]);

//-----------Check the legality of the new position-------------------
//........ First call evaluate and if you see a difference between
//........ 500 and 600 points then one of the kings has been captured
/*
		   double test_val;
		   test_val = gen.EVAL.Evaluate(gen.WHO_PLAY, gen.move_index);
		   if( (test_val >=500 && test_val<600) ){
		      //A king has been captured
			  //Undo the last move and move to the next node


			  gen.undo(gen.move_index - 1);

			  //return -1;
                          continue;
		   }

             */
//-----------Check the legality of the new position-------------------
            gen.val = -AplhaBeta(depth - 1, oside, side, -beta, -alpha);

            gen.undo(gen.move_index - 1);

            //Found the best solution
//System.out.println("Alpha Beta:: Check BETA  beta - val " + beta + " = " + gen.val );
            if (gen.val >= beta) {
                //  if(  (ply-1)<gen.DEPTH  )
                gen.RecordHash(depth, beta, hashBETA, side, gen.PV[depth]);

                gen.BEST_MOVE = gen.PV[gen.DEPTH];
//System.out.println("Alpha Beta:: Found beta" + beta);
//System.out.println("Alpha Beta:: This move caused a beta cutoff " + gen.PV[depth]);
                return beta;
            }

//System.out.println("Alpha Beta:: Check ALPHA  alpha - val " + alpha + " = " + gen.val );
            if (gen.val > alpha) {
//System.out.println("Alpha Beta:: Found better alpha =" + alpha + "- val= " + gen.val);
//System.out.println("Alpha Beta:: This move caused an alpha cutoff " + gen.PV[depth]);
                hashFlag = hashEXACT;
                alpha = gen.val;
                gen.BEST_MOVE = gen.PV[gen.DEPTH];

                for (int d = gen.DEPTH; d >= depth; d--) {
                    gen.LINE += gen.PV[d] + "  ";
                }
//                                  ply score time nodes pv
//                System.out.println("9 156 1084 48000 Nf3 Nc6 Nc3 Nf6");
                gen.score = (int) (gen.val * 100);
                int tm = (int) (gen.tmControl.GetTime() - gen.startTime) * 100;

                gen.pv = ply + " " + gen.score + " " + tm + " " + gen.NODES + " " + gen.LINE;

                /*
				  if(gen.POST)
                    System.out.println(pv);
                 */
                gen.LINE = "";
                if (depth == gen.DEPTH) {

                    gen.BEST_MOVE_INDEX = k;

                    gen.best_from = temp_movelist[k][0];
                    gen.best_to = temp_movelist[k][1];
                    gen.best_type = temp_movelist[k][2];

                    fr = brd.board_squares[brd.board_main[gen.best_from]];
                    t = brd.board_squares[brd.board_main[gen.best_to]];
                    gen.BEST_MOVE = fr + t;

                }

            }

        }
        //....While there are moves on the temporary movelist

        //if(  (ply-1) < gen.DEPTH  )
        gen.RecordHash(depth, alpha, hashFlag, side, gen.PV[depth]);
        return alpha;

    }

//******************************************************
//*         -=       End of AplhaBeta     =-           *
//******************************************************
//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : This method is invoked whenever the prog   *
//*         must start thinking.First creates all      *
//*         pseudolegal moves by calling pseudo_legal()*
//*         method and then chooses the best move      *
//*         via the evaluate() method.                 *
//******************************************************
//*         -=     Start of move_ordering =-           *
//******************************************************
    void move_ordering(int depth, int last, int[][] temp_movelist) throws IOException {
        int temp_0, temp_1, temp_2;
        double temp_val;

        //---------Move ordering for 1rst ply
        if (depth == gen.DEPTH) {

            for (int i = 0; i < (last - 1); i++) {
                for (int j = i + 1; j < (last - 1); j++) {

                    if (gen.WHO_PLAY == gen.LIGHT) {
                        if (gen.eval[i] < gen.eval[j]) {

                            temp_val = gen.eval[i];
                            temp_0 = temp_movelist[i][0];
                            temp_1 = temp_movelist[i][1];
                            temp_2 = temp_movelist[i][2];

                            gen.eval[i] = gen.eval[j];
                            temp_movelist[i][0] = temp_movelist[j][0];
                            temp_movelist[i][1] = temp_movelist[j][1];
                            temp_movelist[i][2] = temp_movelist[j][2];

                            gen.eval[j] = temp_val;
                            temp_movelist[j][0] = temp_0;
                            temp_movelist[j][1] = temp_1;
                            temp_movelist[j][2] = temp_2;

                        }

                    } else if (gen.WHO_PLAY == gen.DARK) {
                        if (gen.eval[i] > gen.eval[j]) {

                            temp_val = gen.eval[i];
                            temp_0 = temp_movelist[i][0];
                            temp_1 = temp_movelist[i][1];
                            temp_2 = temp_movelist[i][2];

                            gen.eval[i] = gen.eval[j];
                            temp_movelist[i][0] = temp_movelist[j][0];
                            temp_movelist[i][1] = temp_movelist[j][1];
                            temp_movelist[i][2] = temp_movelist[j][2];

                            gen.eval[j] = temp_val;
                            temp_movelist[j][0] = temp_0;
                            temp_movelist[j][1] = temp_1;
                            temp_movelist[j][2] = temp_2;

                        }

                    }

                }
            }

        }
        //---------Move ordering for 1rst ply

    }
//******************************************************
//*         -=       End of move_ordering =-           *
//******************************************************

//******************************************************
//*  Type : double                                     *
//*  Args :                                            *
//*  Aim  : Prints the result of the game.             *
//******************************************************
//*         -=     Start of printResult   =-           *
//******************************************************
    double printResult(int depth) throws IOException {

        System.out.println("zero last");
        //Check to deliver mate
        if (gen.find_king(gen.TURN, gen.OTURN)) {
            System.out.println("Delivering !!!!!");
            if (gen.OTURN == gen.LIGHT) {
                System.out.println("hw hates at depth " + depth);
                System.out.println("mating move :" + gen.PV[gen.DEPTH]);
                gen.BEST_MOVE = gen.PV[gen.DEPTH];
                flagMateFound = true;
                return -900.0;

            } else if (gen.OTURN == gen.DARK) {
                System.out.println("lb hates at depth " + depth);
                System.out.println("mating move :" + gen.PV[gen.DEPTH]);
                gen.BEST_MOVE = gen.PV[gen.DEPTH];
                flagMateFound = true;

                return -900.0;

            }
        } else //Check to avoid mate
        if (gen.find_king(gen.OTURN, gen.TURN)) {
            System.out.println("Avoiding !!!!!");

            if (gen.TURN == gen.LIGHT) {
                if (depth == gen.DEPTH) {
                    gen.flagI_am_Mated_B = true;
                }
                System.out.println("hw hates at depth " + depth);
                gen.board_print();
                return -900.0;

            } else if (gen.TURN == gen.DARK) {
                if (depth == gen.DEPTH) {
                    gen.flagI_am_Mated_W = true;
                }
                System.out.println("lb hates at depth " + depth);
                System.out.println("Mating move : " + gen.PV[gen.DEPTH]);
                return -900.0;

            }
        } else if (depth == gen.DEPTH) {
            gen.flagStalemated = true;
            System.out.println("1/2-1/2 {Stalemate}");
            gen.board_print();
            return -1;
        } else {
            System.out.println("Ttalemate");
            System.out.println("Pat move : " + gen.PV[gen.DEPTH]);
            return -1;
        }

        return -1;

    }
//******************************************************
//*         -=     End of printResult       =-          *
//******************************************************

}
