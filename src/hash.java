
import java.util.*;

class hash {

    //-----Useful Hash Flags
    static final int hashEXACT = 0;        // A good move
    static final int hashALPHA = 1;        // An upper bound
    static final int hashBETA = 2;        // A lower bound
    static final double valUNKNOWN = 2000; // This value is compared to the returned value
    // of Alpha Beta search.
    //-----Useful Hash Flags

    long KEY;
    int DEPTH;
    int FLAGS;
    double VALUE;
    String BEST_MOVE;

//******************************************************
//*  Type : public                                     *
//*  Args : long,int,int,int,String                    *
//*  Aim  : Constructur                                *
//******************************************************
//*         -=     Start of hash              =-       *
//******************************************************
    public hash(long KEY, int DEPTH, int FLAGS, double VALUE, String BEST_MOVE) {

        this.KEY = KEY;
        this.DEPTH = DEPTH;
        this.FLAGS = FLAGS;
        this.VALUE = VALUE;
        this.BEST_MOVE = BEST_MOVE;
    }
//******************************************************
//*         -=       End of hash              =-       *
//******************************************************

}
