
import java.io.*;

public class ep {

    generator gen;
    board brd;

//******************************************************
//*  Type : public                                     *
//*  Args :                                            *
//*  Aim  : The constructor of the class.              *
//******************************************************
//*         -=     Start of ep                =-       *
//******************************************************
    public ep(generator gen, board brd) throws IOException {

        this.brd = brd;
        this.gen = gen;

    }
//******************************************************
//*         -=     End   of  ep               =-       *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Generate en passant moves.                 *
//******************************************************
//*         -=     Start of gen_ep           =-        *
//******************************************************
    public void gen_ep() throws IOException {

        if (gen.WHO_PLAY == gen.LIGHT) { //gen.ep_to >=40

            //If the pawn advanced at the right
            if (brd.col[gen.ep_to] != 0 && brd.color[gen.ep_to + 7] == gen.LIGHT && brd.piece[gen.ep_to + 7] == gen.P) {
                gen.movelist_add(gen.ep_to + 7, gen.ep_to, 21);
            }
            //If the pawn advanced at the left
            if (brd.col[gen.ep_to] != 7 && brd.color[gen.ep_to + 9] == gen.LIGHT && brd.piece[gen.ep_to + 9] == gen.P) {
                gen.movelist_add(gen.ep_to + 9, gen.ep_to, 21);
            }

        } else {
            //If the pawn advanced at the right
            if (brd.col[gen.ep_to] != 0 && brd.color[gen.ep_to - 9] == gen.DARK && brd.piece[gen.ep_to - 9] == gen.P) {
                gen.movelist_add(gen.ep_to - 9, gen.ep_to, 21);

            }
            //If the pawn advanced at the left
            if (brd.col[gen.ep_to] != 7 && brd.color[gen.ep_to - 7] == gen.DARK && brd.piece[gen.ep_to - 7] == gen.P) {
                gen.movelist_add(gen.ep_to - 7, gen.ep_to, 21);

            }
        }

    }
//******************************************************
//*         -=       End of gen_ep           =-        *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Make en passant move.                      *
//******************************************************
//*         -=     Start of make_ep      =-            *
//******************************************************
    public void make_ep(int color, int from, int to, String move, int move_type) {

        //zero
        brd.piece[from] = 6;
        brd.color[from] = 6;

        if (color == gen.LIGHT) {

            brd.piece[to + 8] = 6;
            brd.color[to + 8] = 6;

        } else if (color == gen.DARK) {

            brd.piece[to - 8] = 6;
            brd.color[to - 8] = 6;

        }

        //Write the values to the new square
        brd.piece[to] = 0;
        brd.color[to] = color;

        //Backup for the undo method
        gen.History[gen.move_index] = new move(from, to, 0, 6, color, 6, 21, move, gen.castle, gen.ep_to, gen.FIFTY_RULE);

        gen.ep_to = -1;

        gen.move_index++;

    }
//******************************************************
//*         -=       End of make_ep      =-        *
//******************************************************

}
