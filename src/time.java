
import java.util.*;
import java.io.*;

public class time {

//******************************************************
//*  Type : public                                     *
//*  Args :                                            *
//*  Aim  : The constructor of the class.              *
//******************************************************
//*         -=     Start of time              =-       *
//******************************************************
    public time() throws IOException {

    }
//******************************************************
//*         -=     End   of time              =-       *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : This method is invoked whenever the prog   *
//*         needs to know the current time.            *
//******************************************************
//*         -=    Start of GetTime =-                  *
//******************************************************
    long GetTime() throws IOException {

        long H, M, S, total;
        Date today = new Date();
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(today);
        H = cal.get(Calendar.HOUR);
        M = cal.get(Calendar.MINUTE);
        S = cal.get(Calendar.SECOND);
        total = (H * 3600) + (M * 60) + S;

        return total;
    }
//******************************************************
//*         -=     End of GetTime  =-                  *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : This method is invoked to se the CutOff    *
//*         depth                                      *
//******************************************************
//*         -=    Start of TimeOut        =-           *
//******************************************************
    boolean TimeOut(int maxTime, long startTime) throws IOException {

        long now = GetTime();
        if (now >= (startTime + maxTime)) {
            return true;
        }

        return false;
    }
//******************************************************
//*         -=     End of TimeOut  =-                  *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : This method is invoked to se the CutOff    *
//*         depth                                      *
//******************************************************
//*         -=    Start of TimeCutoff     =-           *
//******************************************************
    int TimeCutoff(boolean TimeControl_AllMoves, boolean TimeControl_XMoves,
            int TimeControl_XMoves_Number, int maxTime) throws IOException {

        //Handle  conventional chess clocks: all moves/Xminutes
        if (TimeControl_AllMoves) {
            int desiredMovesNum = 40;
            maxTime = maxTime / desiredMovesNum;

            //Handle  conventional chess clocks:    Xmoves/Xminutes
        } else if (TimeControl_XMoves) {

            maxTime = maxTime / TimeControl_XMoves_Number;

        }

        return maxTime;
    }
//******************************************************
//*         -=     End of TimeCutoff      =-           *
//******************************************************

}
