
class move {

    //-----Useful Hash Flags
    int FROM;
    int TO;
    int PIECE_FROM;
    int PIECE_TO;
    int COLOR_FROM;
    int COLOR_TO;
    int FLAGS;
    int CASTLE;
    int EP;
    int FIFTY;
    String MOVE;

//******************************************************
//*  Type : public                                     *
//*  Args : long,int,int,int,String                    *
//*  Aim  : Constructur                                *
//******************************************************
//*         -=     Start of hash              =-       *
//******************************************************
    public move(int FROM, int TO, int PIECE_FROM, int PIECE_TO, int COLOR_FROM, int COLOR_TO, int FLAGS, String MOVE, int CASTLE, int EP, int FIFTY) {

        this.FROM = FROM;
        this.TO = TO;
        this.FLAGS = FLAGS;
        this.PIECE_FROM = PIECE_FROM;
        this.PIECE_TO = PIECE_TO;
        this.COLOR_FROM = COLOR_FROM;
        this.COLOR_TO = COLOR_TO;
        this.CASTLE = CASTLE;
        this.EP = EP;
        this.FIFTY = FIFTY;
        this.MOVE = MOVE;

    }
//******************************************************
//*         -=       End of hash              =-       *
//******************************************************

}
