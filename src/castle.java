
import java.io.*;

public class castle {

    generator gen;
    board brd;

// I implemented here an idea that I first saw at the source code of
// Tim Kerrigans TSCP.I found it really elegant !
    final static int[] castle_right = {
        7, 15, 15, 15, 3, 15, 15, 11,
        15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15,
        13, 15, 15, 15, 12, 15, 15, 14
    };

//******************************************************
//*  Type : public                                     *
//*  Args :                                            *
//*  Aim  : The constructor of the class.              *
//******************************************************
//*         -=     Start of castle            =-       *
//******************************************************
    public castle(generator gen, board brd) throws IOException {

        this.brd = brd;
        this.gen = gen;

    }
//******************************************************
//*         -=     End   of  castle           =-       *
//******************************************************

    /*
//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Alters the castle rights.                  *
//******************************************************
//*         -=     Start of alter_castle     =-        *
//******************************************************
 public void alter_castle(){
 //Check if the white king has moved
  if(brd.piece[60]!=5 && brd.color[60]!=0){
   gen.w_castle_q=0;
   gen.w_castle_k=0;

  }

 //Check if the white rook from a1 has moved
  if(brd.piece[56]!=3 && brd.color[56]!=0){
   gen.w_castle_q=0;
  }

 //Check if the white rook from h1 has moved
  if(brd.piece[63]!=3 && brd.color[63]!=0){
   gen.w_castle_k=0;
  }

 //Check if the black king has moved
  if(brd.piece[4]!=5 && brd.color[4]!=1){
   gen.b_castle_q=0;
   gen.b_castle_k=0;
   }

 //Check if the black rook from a8 has moved
  if(brd.piece[0]!=3 && brd.color[0]!=1){
   gen.b_castle_q=0;
  }

 //Check if the black rook from h8 has moved
  if(brd.piece[7]!=3 && brd.color[7]!=1){
   gen.b_castle_k=0;
  }
 }
//******************************************************
//*         -=       End of alter_castle    =-         *
//******************************************************
     */
//******************************************************
//*  Type : public boolean                             *
//*  Args :                                            *
//*  Aim  : Check if an opponent piece is attacking    *
//*         a square from my castle side.              *
//******************************************************
//*         -=     Start of check_castle     =-        *
//******************************************************
    public boolean check_castle(int wturn, int castle_type) throws IOException {
        int side, oside;
        boolean check = false;

        side = (wturn == 0) ? 1 : 0;
        oside = (wturn == 0) ? 0 : 1;

        //Check for White o-o
        if (castle_type == 2) {
//----------Test for f1------

            //change the position of the king
            brd.piece[60] = 6;
            brd.color[60] = 6;

            brd.piece[61] = 5;
            brd.color[61] = 0;

            //Validate
            if (gen.WHO_PLAY == gen.LIGHT) {
                check = gen.find_king(0, 1);
            } else if (gen.WHO_PLAY == gen.DARK) {
                check = gen.find_king(1, 0);
            }

            //Undo Changes
            brd.piece[60] = 5;
            brd.color[60] = 0;

            brd.piece[61] = 6;
            brd.color[61] = 6;

            if (check == true) {
                return true;
            }

//----------Test for f1------
//----------Test for g1------
            //change the position of the king
            brd.piece[60] = 6;
            brd.color[60] = 6;

            brd.piece[62] = 5;
            brd.color[62] = 0;

            //Validate
            if (gen.WHO_PLAY == gen.LIGHT) {
                check = gen.find_king(0, 1);
            } else if (gen.WHO_PLAY == gen.DARK) {
                check = gen.find_king(1, 0);
            }

            //Undo Changes
            brd.piece[60] = 5;
            brd.color[60] = 0;

            brd.piece[62] = 6;
            brd.color[62] = 6;

            if (check == true) {
                return true;
            }

//----------Test for g1------
            //Check for White o-o-o
        } else if (castle_type == 3) {
//----------Test for d1------

            //change the position of the king
            brd.piece[60] = 6;
            brd.color[60] = 6;

            brd.piece[59] = 5;
            brd.color[59] = 0;

            //Validate
            if (gen.WHO_PLAY == gen.LIGHT) {
                check = gen.find_king(0, 1);
            } else if (gen.WHO_PLAY == gen.DARK) {
                check = gen.find_king(1, 0);
            }

            //Undo Changes
            brd.piece[60] = 5;
            brd.color[60] = 0;

            brd.piece[59] = 6;
            brd.color[59] = 6;

            if (check == true) {
                return true;
            }

//----------Test for d1------
//----------Test for c1------
            //change the position of the king
            brd.piece[60] = 6;
            brd.color[60] = 6;

            brd.piece[58] = 5;
            brd.color[58] = 0;

            //Validate
            if (gen.WHO_PLAY == gen.LIGHT) {
                check = gen.find_king(0, 1);
            } else if (gen.WHO_PLAY == gen.DARK) {
                check = gen.find_king(1, 0);
            }

            //Undo Changes
            brd.piece[60] = 5;
            brd.color[60] = 0;

            brd.piece[58] = 6;
            brd.color[58] = 6;

            if (check == true) {
                return true;
            }

//----------Test for c1------
            //Check for Black o-o
        } else if (castle_type == 4) {

//----------Test for f8------
            //change the position of the king
            brd.piece[4] = 6;
            brd.color[4] = 6;

            brd.piece[5] = 5;
            brd.color[5] = 1;

            //Validate
            if (gen.WHO_PLAY == gen.LIGHT) {
                check = gen.find_king(0, 1);
            } else if (gen.WHO_PLAY == gen.DARK) {
                check = gen.find_king(1, 0);
            }

            //Undo Changes
            brd.piece[4] = 5;
            brd.color[4] = 1;

            brd.piece[5] = 6;
            brd.color[5] = 6;

            if (check == true) {
                return true;
            }

//----------Test for g8------
//----------Test for g8------
            //change the position of the king
            brd.piece[4] = 6;
            brd.color[4] = 6;

            brd.piece[6] = 5;
            brd.color[6] = 1;

            //Validate
            if (gen.WHO_PLAY == gen.LIGHT) {
                check = gen.find_king(0, 1);
            } else if (gen.WHO_PLAY == gen.DARK) {
                check = gen.find_king(1, 0);
            }

            //Undo Changes
            brd.piece[4] = 5;
            brd.color[4] = 1;

            brd.piece[6] = 6;
            brd.color[6] = 6;

            if (check == true) {
                return true;
            }

//----------Test for g8------
            //Check for Black o-o-o
        } else if (castle_type == 5) {
//----------Test for d8------

            //change the position of the king
            brd.piece[4] = 6;
            brd.color[4] = 6;

            brd.piece[3] = 5;
            brd.color[3] = 1;

            //Validate
            if (gen.WHO_PLAY == gen.LIGHT) {
                check = gen.find_king(0, 1);
            } else if (gen.WHO_PLAY == gen.DARK) {
                check = gen.find_king(1, 0);
            }

            //Undo Changes
            brd.piece[4] = 5;
            brd.color[4] = 1;

            brd.piece[3] = 6;
            brd.color[3] = 6;

            if (check == true) {
                return true;
            }

//----------Test for d8------
//----------Test for c8------
            //change the position of the king
            brd.piece[4] = 6;
            brd.color[4] = 6;

            brd.piece[2] = 5;
            brd.color[2] = 1;

            //Validate
            if (gen.WHO_PLAY == gen.LIGHT) {
                check = gen.find_king(0, 1);
            } else if (gen.WHO_PLAY == gen.DARK) {
                check = gen.find_king(1, 0);
            }

            //Undo Changes
            brd.piece[4] = 5;
            brd.color[4] = 1;

            brd.piece[2] = 6;
            brd.color[2] = 6;

            if (check == true) {
                return true;
            }

//----------Test for c8------
        }

        return false;
    }
//******************************************************
//*         -=       End of check_castle    =-         *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Creates castle moves and adds them to the  *
//*         movelist .                                 *
//******************************************************
//*         -=     Start of castle     =-              *
//******************************************************
    public void castle(int wturn) throws IOException {

        //If White is playing
        if (wturn == 0 && !gen.find_king(0, 1)) {
            //White castles Kingside
            if ((gen.castle & 1) > 0) {
                if ((brd.piece[61] == 6 && brd.piece[62] == 6) && !check_castle(wturn, 2)) {
                    gen.movelist_add(60, 62, 2);   //White king to g1
                }
            }
            //White castles queenside
            if ((gen.castle & 2) > 0) {
                if (((brd.piece[57] == 6 && brd.piece[58] == 6) && brd.piece[59] == 6) && !check_castle(wturn, 3)) {
                    gen.movelist_add(60, 58, 3);   //White king to c1
                }
            }

            //If Black is playing
        } else if (wturn == 1 && !gen.find_king(1, 0)) {
            //Black castles King
            if ((gen.castle & 4) > 0) {
                if ((brd.piece[5] == 6 && brd.piece[6] == 6) && !check_castle(wturn, 4)) {
                    gen.movelist_add(4, 6, 4);   //Black king to g8
                }
            }
            //Black castles queenside
            if ((gen.castle & 8) > 0) {
                if (((brd.piece[1] == 6 && brd.piece[2] == 6) && brd.piece[3] == 6) && !check_castle(wturn, 5)) {
                    gen.movelist_add(4, 2, 5);   //Black king to c8
                }
            }

        }

    }
//******************************************************
//*         -=       End of castle    =-               *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Make castle move.                          *
//******************************************************
//*         -=     Start of make_castle  =-            *
//******************************************************
    public void make_castle(int K_from, int K_to, int R_from, int R_to, int color, String move, int move_type) {

        //zero
        brd.piece[K_from] = 6;
        brd.color[K_from] = 6;
        brd.piece[R_from] = 6;
        brd.color[R_from] = 6;

        //Write the values to the new square
        brd.piece[K_to] = 5;
        brd.color[K_to] = color;
        brd.piece[R_to] = 3;
        brd.color[R_to] = color;

        //Backup for the undo method
        gen.History[gen.move_index] = new move(K_from, K_to, 5, 6, color, 6, move_type, move, gen.castle, gen.ep_to, gen.FIFTY_RULE);

//Update the castle rights
        gen.castle &= castle_right[K_from] & castle_right[K_to];
        // Update ep rights
        gen.ep_to = -1;

        gen.move_index++;

    }
//******************************************************
//*         -=       End of make_castle      =-        *
//******************************************************

}
