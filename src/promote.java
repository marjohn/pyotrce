
import java.io.*;

public class promote {

    generator gen;
    board brd;

//******************************************************
//*  Type : public                                     *
//*  Args :                                            *
//*  Aim  : The constructor of the class.              *
//******************************************************
//*         -=     Start of promote          =-       *
//******************************************************
    public promote(generator gen, board brd) throws IOException {

        this.brd = brd;
        this.gen = gen;

    }
//******************************************************
//*         -=     End   of  promote          =-       *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args : int,int                                    *
//*  Aim  : Generate Promote moves.                    *
//******************************************************
//*         -=     Start of gen_promote      =-        *
//******************************************************
    public void gen_promote(int StartSquare, int EndSquare) throws IOException {

        //Promote to Queen
        if (gen.validate(StartSquare, EndSquare, 6)) {

            brd.movelist[gen.LAST][0] = StartSquare;
            brd.movelist[gen.LAST][1] = EndSquare;
            brd.movelist[gen.LAST][2] = 6;

            ++gen.LAST;
        }
        if (gen.validate(StartSquare, EndSquare, 7)) {

            brd.movelist[gen.LAST][0] = StartSquare;
            brd.movelist[gen.LAST][1] = EndSquare;
            brd.movelist[gen.LAST][2] = 7;

            ++gen.LAST;
        }
        if (gen.validate(StartSquare, EndSquare, 8)) {

            brd.movelist[gen.LAST][0] = StartSquare;
            brd.movelist[gen.LAST][1] = EndSquare;
            brd.movelist[gen.LAST][2] = 8;

            ++gen.LAST;
        }
        if (gen.validate(StartSquare, EndSquare, 9)) {

            brd.movelist[gen.LAST][0] = StartSquare;
            brd.movelist[gen.LAST][1] = EndSquare;
            brd.movelist[gen.LAST][2] = 9;

            ++gen.LAST;
        }
    }
//******************************************************
//*         -=       End of gen_promote      =-        *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Make promote move.                         *
//******************************************************
//*         -=     Start of promote       =-           *
//******************************************************
    public void promote(int from, int to, int color, String move, int move_type) {

        int temp_piece, temp_color;

        //zero & old values
        brd.piece[from] = 6;
        brd.color[from] = 6;
        temp_piece = brd.piece[to];
        temp_color = brd.color[to];

        //Write the values to the new square
        if (move_type == 6) {
            brd.piece[to] = 4;
        }
        if (move_type == 7) {
            brd.piece[to] = 3;
        }
        if (move_type == 8) {
            brd.piece[to] = 1;
        }
        if (move_type == 9) {
            brd.piece[to] = 2;
        }
        brd.color[to] = color;

        //Backup for the undo method
        gen.History[gen.move_index] = new move(from, to, 0, temp_piece, color, temp_color, move_type, move, gen.castle, gen.ep_to, gen.FIFTY_RULE);

        // Update ep rights
        gen.ep_to = -1;

        gen.move_index++;

    }
//******************************************************
//*         -=       End of promote         =-         *
//******************************************************

}
