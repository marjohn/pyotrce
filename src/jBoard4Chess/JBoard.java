package jBoard4Chess;

/**
 * Title: Digital Chess ChessBoard Class</p>
 * Description: This is a class which handles everything related to a chess
 * board</p>
 * Copyright: Copyright (c) 2003-2006</p>
 * Company: Digital Chess Network</p>
 *
 * @author Marountas John
 * @version 1.0
 *
 * This is an intelligent and very friendly Java class which has the following
 * features: 1 Remembers the positions of the pieces 2 Remembers which turn is
 * to play [WORKING] 3 Plays a move 4 Undos a move 5 Keeps a record of the
 * history moves 6 Prints an ASCII board diagram 7 Exports a FEN position 8
 * Remembers En Passant rights 9 Remembers Castle rights 10 Keep Track of 3
 * Repetition rule 11 Keep Track of the 50 move rule 12 Keep Track of the
 * insufficient material rule 13 Knows if a game is in progress or not [DONE]
 *
 * REQUIREMENTS - This library needs the jMove4Chess library
 */
public class JBoard {

    boolean gameStarted;
    public final static int WHITE_TO_PLAY = 0;
    public final static int BLACK_TO_PLAY = 1;
    int sideToPlay;

    public JBoard() {
        // The status of the game
        gameStarted = false;
        // At the start white is about  to make a move
        sideToPlay = WHITE_TO_PLAY;
    }

    public boolean getGameStarted() {
        return gameStarted;
    }

    public void setGameStarted(boolean status) {
        gameStarted = status;
    }

    public int getSideToPlay() {
        return sideToPlay;
    }

    public void setSideToPlay(int sideToPlay) {
        this.sideToPlay = sideToPlay;
    }

    public boolean isWhiteToPlay() {
        if (sideToPlay == WHITE_TO_PLAY) {
            return true;
        }
        return false;
    }

    public boolean isBlackToPlay() {
        if (sideToPlay == BLACK_TO_PLAY) {
            return true;
        }
        return false;
    }

    public void setOtherSideToPlay() {
        if ( isWhiteToPlay() ) {
            sideToPlay = BLACK_TO_PLAY;
        } else {
            sideToPlay = WHITE_TO_PLAY;
        }
    }

    public void reset() {
        setGameStarted(false);
        setSideToPlay(WHITE_TO_PLAY);
    }

    public void debug() {
        System.out.println("-----------------------------------");
        System.out.println("getGameStarted : " + getGameStarted());
        if (isWhiteToPlay()) {
            System.out.println("isWhiteToPlay() : WHITE");
        } else {
            System.out.println("isWhiteToPlay() : BLACK");
        }
        System.out.println("");

    }

}
