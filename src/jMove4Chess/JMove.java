package jMove4Chess;

import java.util.*;

/**
 * This class is intented to be used for parsing chess moves. Especially it will
 * give functionalities like
 *
 * 1. Understanding move notation 2. Parsing Short Algebric 3. Parsing Long
 * Algebric 4. Transforming to one another form 5. Validating input 6. Checking
 * Legality-Availability of the move
 *
 * Requirements: Needs the library jBoard4Chess in order to do the check of
 * Legality-Availability
 *
 * @author Marountas John
 *
 */
public class JMove {

    //Array which transpose the string square given from
    //the user to an index (integer) of the board array.
    static Hashtable squares;

    public JMove() {
        squares = new Hashtable();

        squares.put("a1", "110");
        squares.put("b1", "111");
        squares.put("c1", "112");
        squares.put("d1", "113");
        squares.put("e1", "114");
        squares.put("f1", "115");
        squares.put("g1", "116");
        squares.put("h1", "117");
        squares.put("a2", "98");
        squares.put("b2", "99");
        squares.put("c2", "100");
        squares.put("d2", "101");
        squares.put("e2", "102");
        squares.put("f2", "103");
        squares.put("g2", "104");
        squares.put("h2", "105");
        squares.put("a3", "86");
        squares.put("b3", "87");
        squares.put("c3", "88");
        squares.put("d3", "89");
        squares.put("e3", "90");
        squares.put("f3", "91");
        squares.put("g3", "92");
        squares.put("h3", "93");
        squares.put("a4", "74");
        squares.put("b4", "75");
        squares.put("c4", "76");
        squares.put("d4", "77");
        squares.put("e4", "78");
        squares.put("f4", "79");
        squares.put("g4", "80");
        squares.put("h4", "81");
        squares.put("a5", "62");
        squares.put("b5", "63");
        squares.put("c5", "64");
        squares.put("d5", "65");
        squares.put("e5", "66");
        squares.put("f5", "67");
        squares.put("g5", "68");
        squares.put("h5", "69");
        squares.put("a6", "50");
        squares.put("b6", "51");
        squares.put("c6", "52");
        squares.put("d6", "53");
        squares.put("e6", "54");
        squares.put("f6", "55");
        squares.put("g6", "56");
        squares.put("h6", "57");
        squares.put("a7", "38");
        squares.put("b7", "39");
        squares.put("c7", "40");
        squares.put("d7", "41");
        squares.put("e7", "42");
        squares.put("f7", "43");
        squares.put("g7", "44");
        squares.put("h7", "45");
        squares.put("a8", "26");
        squares.put("b8", "27");
        squares.put("c8", "28");
        squares.put("d8", "29");
        squares.put("e8", "30");
        squares.put("f8", "31");
        squares.put("g8", "32");
        squares.put("h8", "33");
    }

//	******************************************************
//	*  Type :                                            *
//	*  Args : The string of the input move               *
//	*  Aim  : This method will validate the move acording*
//  *         the real current position and the available*
//  *         moves.                                     *
//	******************************************************
//	*         -=     Start of validate_legality    =-    *
//	******************************************************	
    boolean validate_legality(String move) {
        //		 TODO

        // Check if is a legal FROM square (short algebric)
        try {
            Integer.parseInt((String) squares.get(move.substring(0, 2)));
        } catch (Exception ex) {
            return false;
        }

        // Check if is a legal TO square (short algebric)
        try {
            Integer.parseInt((String) squares.get(move.substring(2, 4)));
        } catch (Exception ex) {
            return false;
        }

        return true;
    }

//	******************************************************
//	*  Type :                                            *
//	*  Args : The string of the input move               *
//	*  Aim  : This method will only validate doing some  *
//  *         basic tests.Like  the length of the string.*
//	******************************************************
//	*         -=     Start of validate_input    =-       *
//	******************************************************	
    boolean validate_input(String move) {
        debug("validate_input " + move.length());
        if (move.length() < 4 || move.length() > 4) {
            return false;
        } else {
            return true;
        }
    }

//	******************************************************
//	*  Type : public                                     *
//	*  Args : The string of the input move               *
//	*  Aim  : This method will only mainly call the      *
//  *         appropriate functions to validate the move.*
//	******************************************************
//	*         -=     Start of parse             =-       *
//	******************************************************		
    public boolean parse(String move) {
        if (validate_input(move) && validate_legality(move)) {
            debug("valid move: " + move);
            return true;
        } else {
            debug("invalid move: " + move);
            return false;
        }
    }

    public int getMoveFromSquareInt(String move) {
        try {
            return Integer.parseInt((String) squares.get(move.substring(0, 2)));
        } catch (Exception ex) {
            return 0;
        }
    }

    public int getMoveToSquareInt(String move) {
        try {
            return Integer.parseInt((String) squares.get(move.substring(2, 4)));
        } catch (Exception ex) {
            return 0;
        }
    }

    public int getMoveSetupSquareInt(String move) {
        try {
            return Integer.parseInt((String) squares.get(move.substring(1, 3)));
        } catch (Exception ex) {
            return 0;
        }
    }
    
//******************************************************
//*  Type : method                                     *
//*  Args : message		the string to write    *
//*  Aim  : writes a line to standard output           *                  
//******************************************************
//*         -=       Start of debug    =-              *
//******************************************************    
    public static void debug(String message) {
        System.out.println("# " + message );
        System.out.flush();
    }
//******************************************************
//*         -=        End of debug     =-              *
//******************************************************     
}
