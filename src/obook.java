
import java.lang.System;
import java.util.*;
import java.io.*;

public class obook {

    generator gen;
    String[][] obook;
    int[][] valbook;
    int bookLinesNumber = 0;
    int correctBookLine = 0;
    int tempValue;

//------book
    public boolean inBook = false;
    public String bookMove = "";
    static final int bookMaxLength = 4;
//------book

//******************************************************
//*  Type : public                                     *
//*  Args :                                            *
//*  Aim  : The constructor of the class.              *
//******************************************************
//*         -=     Start of obook              =-       *
//******************************************************
    public obook(generator gen) throws IOException {

        this.gen = gen;
        obook = new String[100][bookMaxLength + 1];
        valbook = new int[100][bookMaxLength + 1];
        if (gen.FEN == false) {
            readBook();
        }

    }
//******************************************************
//*         -=     End   of obook              =-       *
//******************************************************

//******************************************************
//*  Type : public                                     *
//*  Args :                                            *
//*  Aim  : The constructor of the class.              *
//******************************************************
//*         -=     Start of readBook          =-       *
//******************************************************
    public void readBook() throws IOException {
        StringTokenizer st;

        String move;
        int value;

        FileReader frBook;
        try{
            frBook = new FileReader("book.txt");
        }catch(FileNotFoundException ex){
            frBook = new FileReader("../book.txt");
        }

        LineNumberReader lnrBook = new LineNumberReader(frBook);

        String s;

        while ((s = lnrBook.readLine()) != null) {
            st = new StringTokenizer(s);

            bookLinesNumber = lnrBook.getLineNumber();

            if (s.charAt(0) == '#') {
                if (bookLinesNumber != 0) {
                    bookLinesNumber--;
                }
                continue;
            }

            String debug ="";
            for (int j = 0; j < bookMaxLength; j++) {
                String fromBook = st.nextToken();
                debug += "j["+j+"]";

                if (fromBook.length() == 6) {
                    move = (String) fromBook.substring(0, 4);
                    value = Integer.parseInt(fromBook.substring(4, 6));
                } else if (fromBook.length() == 5) {
                    move = (String) fromBook.substring(0, 3);
                    value = Integer.parseInt(fromBook.substring(3, 5));
                } else if (fromBook.length() == 4) {
                    move = fromBook;
                    value = 0;
                } else {
                    continue;
                }

                obook[bookLinesNumber - 1][j] = move;
                valbook[bookLinesNumber - 1][j] = value;
            }
            System.out.println(debug);

            System.out.println(bookLinesNumber + " : " + s);

        }

        lnrBook.close();
        frBook.close();

    }
//******************************************************
//*         -=     End   of readBook           =-       *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : This method is invoked whenever the prog   *
//*         must start thinking.First creates all      *
//*         pseudolegal moves by calling pseudo_legal()*
//*         method and then chooses the best move      *
//*         via the evaluate() method.                 *
//******************************************************
//*         -=     Start of book          =-           *
//******************************************************
    public void book() throws IOException, ArrayIndexOutOfBoundsException {
        System.out.println("book() move_index:" + gen.move_index);
        int var = 0;
        if (gen.move_index == 0) {
            inBook = true;

            Random r = new Random();
            double chance = r.nextDouble();

            if (chance >= 0.6) {
                bookMove = "e2e4";
            } else if (chance >= 0.4) {
                bookMove = "c2c4";
            } else if (chance >= 0.3) {
                bookMove = "g1f3";
            } else {
                bookMove = "d2d4";
            }
            gen.best_type = 1;
        } else if (gen.move_index >= 1 && gen.move_index <= bookMaxLength) {

            //Find the index of the lines that have the first move
            int[] varLinesFound = new int[bookLinesNumber];
            int varIndex = 0;
            String debug = "";
            for (int i = 0; i <= bookLinesNumber; i++) {
                debug += "line["+i+"]: i("+gen.History[0].MOVE+") ? ("+obook[i][0]+")\n";
                if (gen.History[0].MOVE.equals(obook[i][0])) {

                    varLinesFound[varIndex] = i;
                    varIndex++;

                }
            }
            System.out.println(debug);

            //____Check if the tree is followed
            int[] bookList = new int[bookLinesNumber];
            int bookListIndex = 0;

            try {
                debug = "";
                for (int j = 0; j < varIndex; j++) {
                    debug += j+":";
                    for (int i = 0; i < gen.move_index; i++) {
                        debug += " m("+i+")";
                        debug += " H("+gen.History[i].MOVE+") ? ("+obook[varLinesFound[j]][i]+")\n";
                        if (gen.History[i].MOVE.equals(obook[varLinesFound[j]][i])) {
                            inBook = true;

                        } else {
                            inBook = false;
                            break;
                        }
                    }
                    if (inBook) {

                        bookList[bookListIndex] = varLinesFound[j];
                        bookListIndex++;
                    }

                }
                System.out.println(debug);
            } catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("# My book  is too small to handle this line ! sorry :( ");
            }
            if (bookListIndex != 0) {
                inBook = true;
            }

            if (inBook) {

                if (bookListIndex > 1) {
                    tempValue = valbook[bookList[0]][gen.move_index];
                    correctBookLine = bookList[0];
                    for (int k = 1; k < bookListIndex; k++) {
                        if (valbook[bookList[k]][gen.move_index] > tempValue) {
                            tempValue = valbook[bookList[k]][gen.move_index];
                            correctBookLine = bookList[k];
                        }
                    }

                } else {
                    correctBookLine = bookList[0];
                }

                bookMove = obook[correctBookLine][gen.move_index];
                gen.best_type = 1;
            }
            //____Check if the tree is followed

        } else {
            inBook = false;
        }

    }
//******************************************************
//*         -=       End of book              =-       *
//******************************************************

}
