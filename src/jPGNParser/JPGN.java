package jPGNParser;

import java.lang.System;
import java.util.*;
import java.io.*;

/**
 *
 * @author Marountas John
 *
 * The PGN must be in SAN notation The PGN must not have subvariations or
 * comments
 */
public class JPGN {

    StringTokenizer st;
    FileReader frBook;
    LineNumberReader lnrBook;
    int bookLinesNumber = 0;
    int bookGamesNumber = 0;
    int bookMovesNumber = 0;
    int maxGames = 4;
    String[] games;
    String[][] moves;

    public JPGN(String filename) {

        try {
            frBook = new FileReader(filename);
            lnrBook = new LineNumberReader(frBook);
        } catch (Exception ex) {
            System.out.println("Error on opening pgn file.");
        }
        // Initialize games
        games = new String[maxGames];
        for (int i = 0; i < maxGames; i++) {
            games[i] = "";
        }
        moves = new String[maxGames][2];
        for (int i = 0; i < maxGames; i++) {
            moves[i][0] = "";
            moves[i][1] = "";
        }
    }

    public void read() {
        String s;
        try {
            while ((s = lnrBook.readLine()) != null) {
                st = new StringTokenizer(s, " ");
                String firstToken;
                try {
                    firstToken = st.nextToken();
                } catch (Exception ex) {
                    firstToken = " ";
                    continue;
                }
                if (firstToken.equals("[Result")) {
                    bookGamesNumber++;
                    bookMovesNumber = 0;
                    if (bookGamesNumber > maxGames) {
                        bookGamesNumber--;
                        break;
                    }
                }
                // Store token
                // if it is a TAG
                if (s.length() >= 1 && s.charAt(0) == '[') {
                    continue;
                } else {

                    if (firstToken.trim().length() >= 1 && firstToken.charAt(firstToken.trim().length() - 1) == '.') {
                        bookMovesNumber++;
                    } else {
                        moves[bookGamesNumber - 1][1] += firstToken + ",";
                    }
                    while (st.hasMoreTokens()) {
                        String tmp = st.nextToken();
                        if (tmp.trim().length() >= 1 && tmp.charAt(tmp.trim().length() - 1) == '.') {
                            bookMovesNumber++;
                        } else {
                            moves[bookGamesNumber - 1][1] += tmp + ",";
                            moves[bookGamesNumber - 1][0] = "" + String.valueOf(bookMovesNumber);
                        }

                    }
                }
                bookLinesNumber = lnrBook.getLineNumber();

            }
            for (int i = 0; i < bookGamesNumber; i++) {

                System.out.println(i + " : ( " + moves[i][0] + ")" + moves[i][1]);
            }
            System.out.println("-------STATS---------");
            System.out.println("Lines : " + bookLinesNumber);
            System.out.println("Games : " + bookGamesNumber);
            System.out.println("-------STATS---------");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void sort() {
        //TODO
    }

    public void export() {
        //TODO
    }
}
