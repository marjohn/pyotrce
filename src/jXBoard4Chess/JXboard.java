package jXBoard4Chess;

import java.util.*;

/**
 * This is an interface for the Chess Engine Protocol that is used by xboard
 *
 * @author marjohn
 */
public class JXboard {

    private Map<String, String> features;

    private String[] known_commands;

    private int protocol_version;

    private String accepted_features;
    private String rejected_features;

    public JXboard(String[] features) {
        this.features = new HashMap<String, String>();
        accepted_features = "";
        rejected_features = "";

        this.protocol_version = 1;

        String feature = "";
        for (int i = 0; i < features.length; i++) {
            feature = features[i];
            String[] tmp = feature.split("=");
            this.features.put(tmp[0], tmp[1]);
        }

        known_commands = new String[]{
            "xboard",
            "new",
            "quit",
            "force",
            "white",
            "black",
            "time",
            "otim",
            "level",
            "post",
            "nopost",
            "edit",
            "go",
            "protover",
            "accepted",
            "rejected",
            "random",
            "hard",
            "easy"
        };

    }

    public void send_features() {
        String my_features = "";
        for (Map.Entry<String, String> entry : features.entrySet()) {
            my_features += entry.getKey() + "=" + entry.getValue() + " ";
        }
        write("feature " + my_features);
    }

    public String[] identify(String line) {
        StringTokenizer st = new StringTokenizer(line);
        String cmd = st.nextToken();

        //[command,action,data]
        String[] command = {"false", cmd, "continue", ""};
        for (int i = 0; i < known_commands.length; i++) {
            if (cmd.equals(known_commands[i])) {
                String data = "";
                while (st.hasMoreTokens()) {
                    if (data.equals("")) {
                        data = st.nextToken();
                    } else {
                        data += "," + st.nextToken();
                    }
                }
                command = new String[]{"true", cmd, "continue", data};
                break;
            }
        }
        return command;
    }

    public void set_protocol_version(String version) {
        this.protocol_version = Integer.parseInt(version);
    }

    public void set_accepted_feature(String feature) {

        if (accepted_features.equals("")) {
            accepted_features = feature;
        } else {
            accepted_features += "," + feature;
        }
    }
    
    public void set_rejected_feature(String feature) {

        if (rejected_features.equals("")) {
            rejected_features = feature;
        } else {
            rejected_features += "," + feature;
        }
    }    

//******************************************************
//*  Type : method                                     *
//*  Args : message		the string to write    *
//*  Aim  : rites a line to standard output            *                  
//******************************************************
//*         -=       Start of write    =-              *
//******************************************************    
    public static void write(String message) {
        System.out.println(message);
        System.out.flush();
    }
//******************************************************
//*         -=        End of write     =-              *
//******************************************************       

}
