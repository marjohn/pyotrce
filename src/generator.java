
import java.util.*;
import java.io.*;

public class generator {

    //Declaring the var of other classes
    board brd; // board.class-----> Handles the representation of the board
    evaluate EVAL; // evaluate.class--> The evaluation function
    ep EP; // ep.class--------> Handles all en passant methods
    castle CASTLE; // castle.class----> Handles the castling operation
    promote PROMOTE; // promote.class---> The promotion of a pawn
    obook OBOOK; // obook.class-----> The opening book
    search SEARCH; // search.class----> Holds all search functions
    time tmControl; // time.class------> Time management
    hash[] HashTable; // hash.class------> All about hashing
    move[] History; // move.class------> An array that stores all moves already played.Useful when undoing.

    int random = 0; // Use or not any randomness
    int pondering = 0; // Use podering during play

    //-------Declaring vars for storing information about the move--------
    //....The best move
    int BEST_MOVE_INDEX; // This is the index of the best move inside the
    // movelist array.
    int best_from, best_to, best_type; // Stores the indeces of the squares of the best move
    String BEST_MOVE = null; //Stores the string of the best move

    //....The last move
    int move_index; //Here we store the index of the last move
    int LAST = 0, last = 1; //Here we store the number of candidates moves after a call from pseudo_legal()

    //....An en passant move
    int ep_to; // the Square of the en passant move

    //-------Declaring vars for storing information about the move--------
    static final int TABLE_SIZE = 131072;

    boolean FEN = false;

    boolean flagI_am_Mated_W = false;
    boolean flagI_am_Mated_B = false;
    boolean flagStalemated = false;
    boolean eval_ = false;
    int castle;

    long[][][] zobrist = new long[2][13][64];

    int SEARCH_PLY = 0; //Used in the AlphaBeta
    boolean inNullMove = false;
    int DEPTH = 4;
    String LINE = "";
    String[] PV;
    String pv = "9 156 1084 48000 Nf3";
    int score;
    long NODES = 0;
    boolean POST = false;

//-----------Time Controls
    int maxTime = 60;
    int maxoTime = 60;
    long startTime, nowTime;
    int TimeControl_XMoves_Number;
    boolean TimeControl_AllMoves = true;
    boolean TimeControl_XMoves = false;
//-----------Time Controls

    int best = 0;
    double val = 0.0, alpha, beta;

    long[] repeat_board;
    double[] eval;

    int[] direction = { //How many direction each piece can move?
        0, 8, 4, 4, 8, 8
    };

    int FIFTY_RULE = 0;

    String MOVE;

    int WHO_PLAY;
    int TURN, OTURN; // Who's turn is.Takes two modes: 0-white and 1-black */

    // These are for checking the pieces
    static final int P = 0; //..Pawns
    static final int N = 1; //..Knight
    static final int B = 2; //..Bishop
    static final int R = 3; //..Rook
    static final int Q = 4; //..Queen
    static final int K = 5; //..King

    //Shows the state of a square
    static final int EMPTY = 6;
    static final int LIGHT = 0;
    static final int DARK = 1;

//******************************************************
//*  Type : public                                     *
//*  Args :                                            *
//*  Aim  : The constructor of the class.              *
//******************************************************
//*         -=     Start of generator         =-       *
//******************************************************
    public generator() throws IOException {

        //...Initializing all classes vars
        History = new move[1000];
        for (int i = 0; i < 1000; i++) {
            History[i] = new move(-1, -1, -1, -1, -1, -1, -1, "", 15, -1, 0);

        }
        HashTable = new hash[TABLE_SIZE];

        for (int i = 0; i < TABLE_SIZE; i++) {
            HashTable[i] = new hash(-1, -1, -1, -1, "");
        }
        brd = new board();
        EVAL = new evaluate(this.brd, this);
        EP = new ep(this, this.brd);
        CASTLE = new castle(this, this.brd);
        PROMOTE = new promote(this, this.brd);
        SEARCH = new search(this, this.brd);
        OBOOK = new obook(this);
        tmControl = new time();
        //...Initializing all classes vars

        FEN = false;

        move_index = 0;

        repeat_board = new long[1000];
        eval = new double[1000];
        PV = new String[10];

//Setting the castling status
        castle = 15;
//Setting the ep right
        ep_to = -1;

        // Fill up zobrist keys with random  numbers
        Random r = new Random();

        for (int c = LIGHT; c <= DARK; c++) {
            for (int p = P; p <= K; p++) {
                for (int l = 0; l < 64; l++) {
                    zobrist[c][p][l] = r.nextLong();
//		  System.out.println("  " + zobrist[c][p][l]);
                }
            }
        }

    }

//******************************************************
//*         -=       End of generator         =-       *
//******************************************************
//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : This method is invoked whenever the prog   *
//*         must start thinking.First creates all      *
//*         pseudolegal moves by calling pseudo_legal()*
//*         method and then chooses the best move      *
//*         via the evaluate() method.                 *
//******************************************************
//*         -=     Start of brain         =-           *
//******************************************************
    public void brain() throws IOException {

        //Initializing Flags and vars
        BEST_MOVE_INDEX = 0;
        best = 0;
        SEARCH_PLY = 0;

        //Check to see if the game is still in the opening phase
        if (move_index < OBOOK.bookMaxLength && FEN == false) {
            OBOOK.book();

            //-----If a bookmove is found,just play it without a second thougth !------
            if (OBOOK.inBook && OBOOK.bookMove!=null && OBOOK.bookMove.length() > 1) {
                System.out.println("in book " + move_index);
                //From square
                best_from = Integer.parseInt((String) brd.squares.get(
                        OBOOK.bookMove.substring(0, 2)));
                best_from = brd.board_frame[best_from];
                //To square
                best_to = Integer.parseInt((String) brd.squares.get(
                        OBOOK.bookMove.substring(2, 4)));
                best_to = brd.board_frame[best_to];

                fifty_rule(brd.board_main[best_from],
                        brd.board_main[best_to]);
                make_move(brd.board_main[best_from],
                        brd.board_main[best_to], OBOOK.bookMove,
                        best_type);

                //Repetition Detection
                if (repetition_rule()) {
                    System.out.println("1/2-1/2 {Draw by repetition}");
                    return;
                }

                board_print();
                return;
                //-----If a bookmove is found,just play it without a second thougth !------

                //-----We are in the opening phase but NO book move was found :-(
                //      So just search normally
            } else {
                SEARCH.search();
            }
            //-----So we are in the middle game or endgame phase, so search normally-------
        } else {
            SEARCH.search();
        }

        if (flagI_am_Mated_W) { //Pyotr is mated by Black
            System.out.println("0-1 {Black mates}");
            return;
        }
        if (flagI_am_Mated_B) { //Pyotr is mated by White
            System.out.println("1-0 {White mates}");
            return;
        }
        if (flagStalemated) { //Pyotr is stalemated
            System.out.println("1/2-1/2 {Stalemate}");
            return;
        }

        if (FIFTY_RULE >= 100) {
            System.out.println("1/2-1/2 {Draw by fifty move rule}");

        } else {
            String fr = brd.board_squares[brd.board_main[best_from]];
            String t = brd.board_squares[brd.board_main[best_to]];
            String move = fr + t;

            fifty_rule(brd.board_main[best_from],
                    brd.board_main[best_to]);
            make_move(brd.board_main[best_from], brd.board_main[best_to],
                    move, best_type);

//Repetition Detection
            if (repetition_rule()) {
                System.out.println("1/2-1/2 {Draw by repetition}");

            }
            board_print();

        }

    }

//******************************************************
//*         -=       End of brain         =-           *
//******************************************************
//******************************************************
//*  Type : double                                     *
//*  Args :                                            *
//*  Aim  : Creates the ZobristKey.                    *
//******************************************************
//*         -=     Start of ZobristKey    =-           *
//******************************************************
    int ZobristKey(int side) throws IOException {
        int hashKey = 0;

        for (int p = 0; p < 64; p++) {
            hashKey ^= zobrist[side][brd.piece[p]][p];

        }
        return hashKey;
    }

//******************************************************
//*         -=     End of ZobristKey        =-          *
//******************************************************
//******************************************************
//*  Type : double                                     *
//*  Args :                                            *
//*  Aim  : Searching the HashTable                    *
//******************************************************
//*         -=     Start of SearchHash    =-           *
//******************************************************
    double SearchHash(int depth, double alpha, double beta, int side) throws
            IOException {

        // Find the board's hash position in Table
        int key = Math.abs(ZobristKey(side) % TABLE_SIZE);

        // If the entry is an empty placeholder, we don't have a match
        if (HashTable[key].FLAGS == -1) { // jcTranspositionEntry.NULL_ENTRY )
            return HashTable[key].valUNKNOWN;
        }

        // Check for a hashing collision!
        if (HashTable[key].KEY != ZobristKey(side)) {
            return HashTable[key].valUNKNOWN;
        }

        // Now, we know that we have a match!  Copy it into the output parameter
        // and return
        if (HashTable[key].DEPTH >= depth) {

            if (HashTable[key].FLAGS == HashTable[key].hashEXACT) {
//		board_print();
                //System.out.println("SearchHash :: Found exact value " +HashTable[ i ].DEPTH + " - " + depth);
                return HashTable[key].VALUE;
            }

            if ((HashTable[key].FLAGS == HashTable[key].hashALPHA)
                    && (HashTable[key].VALUE <= alpha)) {
//		board_print();
                //System.out.println("SearchHash :: Found alpha value");
                return alpha;
            }

            if ((HashTable[key].FLAGS == HashTable[key].hashBETA)
                    && (HashTable[key].VALUE >= beta)) {
//		board_print();
                //System.out.println("SearchHash :: Found beta value");
                return beta;
            }

        }

        return HashTable[key].valUNKNOWN;
    }

//******************************************************
//*         -=     End of SearchHash       =-          *
//******************************************************
//******************************************************
//*  Type : double                                     *
//*  Args :                                            *
//*  Aim  : Add entry to the HashTable                 *
//******************************************************
//*         -=     Start of RecordHash    =-           *
//******************************************************
    void RecordHash(int ttdepth, double val, int FLAG, int side,
            String move) throws IOException {

        // Find the board's hash position in Table
        int key = Math.abs(ZobristKey(side) % TABLE_SIZE);

        HashTable[key] = new hash(ZobristKey(side), ttdepth, FLAG, val,
                move);
    }

//******************************************************
//*         -=     End of RecordHash       =-          *
//******************************************************
//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Create all the pseudo legal moves          *
//******************************************************
//*         -=     Start of pseudo_legal     =-        *
//******************************************************
    public void pseudo_legal(int wturn, int woturn) {

        try {
            LAST = 0;

            WHO_PLAY = wturn;

            // Create all castle possible moves
            CASTLE.castle(wturn);

            //Create en passant moves if ep_to=-1
            if (ep_to != -1) {
                EP.gen_ep();

            }

            for (int i = 0; i < 64; ++i) {

                if (brd.color[i] == wturn) { //Check for the sides to move pieces

                    if (brd.piece[i] == P) { //Create the Pawns moves

                        if (wturn == LIGHT) { // Create the moves for the white pawns

                            if (brd.col[i] != 0 && brd.color[i - 9] == DARK) { // Eat left
                                if (brd.row[i] == 6) {
                                    PROMOTE.gen_promote(i, i - 9);
                                } else {
                                    movelist_add(i, i - 9, 1);
                                }
                            }
                            if (brd.col[i] != 7 && brd.color[i - 7] == DARK) { // Eat Right
                                if (brd.row[i] == 6) {
                                    PROMOTE.gen_promote(i, i - 7);
                                } else {
                                    movelist_add(i, i - 7, 1);
                                }
                            }

                            if (brd.color[i - 8] == EMPTY) {
                                if (brd.row[i] == 6) {
                                    PROMOTE.gen_promote(i, i - 8);
                                } else {
                                    movelist_add(i, i - 8, 1); //One forward
                                }
                                if (brd.row[i] == 1 && brd.color[i - 16] == EMPTY) {
                                    movelist_add(i, i - 16, 24); //Two forward

                                }
                            }

                        } else { // Create the moves for the black pawns

                            if (brd.col[i] != 0 && brd.color[i + 7] == LIGHT) { // Eat left
                                if (brd.row[i] == 1) {
                                    PROMOTE.gen_promote(i, i + 7);
                                } else {
                                    movelist_add(i, i + 7, 1);
                                }
                            }
                            if (brd.col[i] != 7 && brd.color[i + 9] == LIGHT) { // Eat Right
                                if (brd.row[i] == 1) {
                                    PROMOTE.gen_promote(i, i + 9);
                                } else {
                                    movelist_add(i, i + 9, 1);
                                }
                            }

                            if (brd.color[i + 8] == EMPTY) {
                                if (brd.row[i] == 1) {
                                    PROMOTE.gen_promote(i, i + 8);
                                } else {
                                    movelist_add(i, i + 8, 1); //One forward
                                }

                                if (brd.row[i] == 6 && brd.color[i + 16] == EMPTY) {
                                    movelist_add(i, i + 16, 24); //Two forward
                                }
                            }

                        }

                    } else { // Create the others moves
                        // Acordindg to the direction that the piece
                        // can go
                        for (int j = 0; j < direction[brd.piece[i]]; ++j) {

                            for (int ENDSQUARE = i;;) {
                                //Create the new endSquare
                                ENDSQUARE = brd.board_frame[brd.board_main[ENDSQUARE]
                                        + brd.piece_move[brd.piece[i]][j]];

                                //Validate endSquare.Check it is in the borders of the board
                                if (ENDSQUARE == -1) {
                                    break;
                                }

                                //Capture move
                                if (brd.color[ENDSQUARE] != EMPTY) {
                                    if (brd.color[ENDSQUARE] == woturn) {
                                        movelist_add(i, ENDSQUARE, 1);
                                    }
                                    break;
                                }

                                //Empty Squares
                                movelist_add(i, ENDSQUARE, 1);

                                //Check for brd.slide
                                if (!brd.slide[brd.piece[i]]) {
                                    break;
                                }
                            }
                        }

                    }

                }

            }

        } catch (FileNotFoundException fex) {
        } catch (IOException ex) {
            System.out.println(ex);
        }
        //Debug
    }

//******************************************************
//*         -=       End of pseudo_legal     =-        *
//******************************************************
//******************************************************
//*  Type : public void                                *
//*  Args : int,int,int                                *
//*  Aim  : Add a move to the possible moves list.     *
//******************************************************
//*         -=     Start of movelist_add     =-        *
//******************************************************
    public void movelist_add(int StartSquare, int EndSquare,
            int MoveType) throws IOException {

        int SEARCH_PLY = 1;
        if (SEARCH_PLY == 1 || inNullMove) { //If we are in the first ply of the search
            //Check only the legal moves

            if (validate(StartSquare, EndSquare, MoveType)) {

                brd.movelist[LAST][0] = StartSquare;
                brd.movelist[LAST][1] = EndSquare;
                brd.movelist[LAST][2] = MoveType;

                ++LAST;
            }
        } else {
            brd.movelist[LAST][0] = StartSquare;
            brd.movelist[LAST][1] = EndSquare;
            brd.movelist[LAST][2] = MoveType;

            ++LAST;

        }

    }

//******************************************************
//*         -=       End of movelist_add     =-        *
//******************************************************
//******************************************************
//*  Type : public boolean                             *
//*  Args :  int,int,int                               *
//*  Aim  : Validate a  possible move.                 *
//******************************************************
//*         -=     Start of validate     =-            *
//******************************************************
    public boolean validate(int StartSquare, int EndSquare,
            int MoveType) throws IOException {

        boolean check = false;

        String f = brd.board_squares[brd.board_main[StartSquare]];
        String t = brd.board_squares[brd.board_main[EndSquare]];
        String move = f + t;

        make_move(brd.board_main[StartSquare], brd.board_main[EndSquare],
                move, MoveType);

        if (WHO_PLAY == LIGHT) {
            check = find_king(0, 1);
        } else if (WHO_PLAY == DARK) {
            check = find_king(1, 0);
        }

        if (SEARCH_PLY <= 1) {
            eval[LAST] = EVAL.Evaluate(WHO_PLAY, move_index);

        }
        undo(move_index - 1);

        if (check == true) {
            return false;
        }
        return true;
    }

//******************************************************
//*         -=       End of validate     =-            *
//******************************************************
//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Prints all  the possible moves .           *
//******************************************************
//*         -=     Start of movelist_print     =-      *
//******************************************************
    public void movelist_print() {

        pseudo_legal(OTURN, TURN);
        int f = 0;
        for (int j = 0; j <= (LAST - 1); j++) {

            System.out.print(""
                    + brd.board_squares[brd.board_main[brd.movelist[j][0]]]
                    + ""
                    + brd.board_squares[brd.board_main[brd.movelist[j][1]]] + " _ ");
            if (f > 5) {
                f = 0;
                System.out.println("");
            }
            f++;
        }

    }

//******************************************************
//*         -=       End of movelist_print     =-      *
//******************************************************
//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Prints  the  moves played so far.          *
//******************************************************
//*         -=     Start of history_print     =-       *
//******************************************************
    public void history_print() {
        int in = 1;
        for (int j = 0; j <= (move_index - 1); j++) {
            System.out.println(in++ + "." + History[j++].MOVE + ","
                    + History[j].MOVE);
        }

    }

//******************************************************
//*         -=       End of history_print    =-        *
//******************************************************
//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Prints  the  diagram.                      *
//******************************************************
//*         -=     Start of board_print     =-         *
//******************************************************
    public void board_print() {
        int g, r;

        if (OTURN == DARK) {
            g = 0;
            r = 8;
            System.out.println();
            System.out.println("    +---+---+---+---+---+---+---+---+");
            System.out.print(" " + r + " ");
            for (int h = 0; h < 64; h++) {
                g++;

                if (brd.color[h] == 0) {
                    System.out.print(" | " + brd.w_letter[brd.piece[h]]);
                } else {
                    System.out.print(" | " + brd.b_letter[brd.piece[h]]);
                }

                if (g == 8) {
                    r--;
                    System.out.print(" | ");
                    System.out.println();
                    System.out.println("    +---+---+---+---+---+---+---+---+");
                    if (r > 0) {
                        System.out.print(" " + r + " ");
                    }
                    g = 0;
                }
            }

            System.out.println("      a   b   c   d   e   f   g   h  ");
        } else if (OTURN == LIGHT) {
            g = 0;
            r = 1;
            System.out.println();
            System.out.println("    +---+---+---+---+---+---+---+---+");
            System.out.print(" " + r + " ");
            for (int h = 63; h >= 0; h--) {
                g++;

                if (brd.color[h] == 0) {
                    System.out.print(" | " + brd.w_letter[brd.piece[h]]);
                } else {
                    System.out.print(" | " + brd.b_letter[brd.piece[h]]);
                }

                if (g == 8) {
                    r++;
                    System.out.print(" | ");
                    System.out.println();
                    System.out.println("    +---+---+---+---+---+---+---+---+");
                    if (r < 9) {
                        System.out.print(" " + r + " ");
                    }
                    g = 0;
                }
            }

            System.out.println("      h   g   f   e   d   c   b   a  ");
        }

    }

//******************************************************
//*         -=       End of board_print    =-          *
//******************************************************
//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Make undo move.                            *
//******************************************************
//*         -=     Start of undo    =-                 *
//******************************************************
    public void undo(int undo_move) {

        if (History[undo_move].FLAGS == 1
                || History[undo_move].FLAGS == 24) { //A normal move
            //From square
            brd.piece[brd.board_frame[History[undo_move].FROM]] = History[undo_move].PIECE_FROM;
            brd.color[brd.board_frame[History[undo_move].FROM]] = History[undo_move].COLOR_FROM;

            //Write the values to the new square
            brd.piece[brd.board_frame[History[undo_move].TO]] = History[undo_move].PIECE_TO;
            brd.color[brd.board_frame[History[undo_move].TO]] = History[undo_move].COLOR_TO;

            // update castle rights
            castle = History[undo_move].CASTLE;
            // Update ep rights
            ep_to = History[undo_move].EP;

            History[undo_move].MOVE = null;
            move_index--;
        } else if (History[undo_move].FLAGS == 2) { //White o-o

            //Put the king back
            brd.piece[60] = 5;
            brd.color[60] = 0;
            brd.piece[62] = 6;
            brd.color[62] = 6;

            //Put the rook back
            brd.piece[63] = 3;
            brd.color[63] = 0;
            brd.piece[61] = 6;
            brd.color[61] = 6;

            History[undo_move].MOVE = null;
            move_index--;

            //Alter the castle status
            castle = History[undo_move].CASTLE;
            // Update ep rights
            ep_to = History[undo_move].EP;

        } else if (History[undo_move].FLAGS == 3) { //White o-o-o

            //Put the king back
            brd.piece[60] = 5;
            brd.color[60] = 0;
            brd.piece[58] = 6;
            brd.color[58] = 6;

            //Put the rook back
            brd.piece[56] = 3;
            brd.color[56] = 0;
            brd.piece[59] = 6;
            brd.color[59] = 6;

            History[undo_move].MOVE = null;
            move_index--;

            //Alter the castle status
            castle = History[undo_move].CASTLE;
            // Update ep rights
            ep_to = History[undo_move].EP;

        } else if (History[undo_move].FLAGS == 4) { //Black o-o

            //Put the king back
            brd.piece[4] = 5;
            brd.color[4] = 1;
            brd.piece[6] = 6;
            brd.color[6] = 6;

            //Put the rook back
            brd.piece[7] = 3;
            brd.color[7] = 1;
            brd.piece[5] = 6;
            brd.color[5] = 6;

            History[undo_move].MOVE = null;
            move_index--;

            //Alter the castle status
            castle = History[undo_move].CASTLE;
            // Update ep rights
            ep_to = History[undo_move].EP;

        } else if (History[undo_move].FLAGS == 5) { //Black o-o-o

            //Put the king back
            brd.piece[4] = 5;
            brd.color[4] = 1;
            brd.piece[2] = 6;
            brd.color[2] = 6;

            //Put the rook back
            brd.piece[0] = 3;
            brd.color[0] = 1;
            brd.piece[3] = 6;
            brd.color[3] = 6;

            History[undo_move].MOVE = null;
            move_index--;

            //Alter the castle status
            castle = History[undo_move].CASTLE;
            // Update ep rights
            ep_to = History[undo_move].EP;

        } else if (History[undo_move].FLAGS == 6
                | History[undo_move].FLAGS == 7
                | History[undo_move].FLAGS == 8
                | History[undo_move].FLAGS == 9) { //Undo A promotion
            //From square

            brd.piece[History[undo_move].FROM] = History[undo_move].PIECE_FROM;
            brd.color[History[undo_move].FROM] = History[undo_move].COLOR_FROM;

            //Write the values to the new square
            brd.piece[History[undo_move].TO] = History[undo_move].PIECE_TO;
            brd.color[History[undo_move].TO] = History[undo_move].COLOR_TO;

            // update castle rights
            castle = History[undo_move].CASTLE;
            // Update ep rights
            ep_to = History[undo_move].EP;

            History[undo_move].MOVE = null;
            move_index--;
        } else if (History[undo_move].FLAGS == 21) { //ep undo

            if (History[undo_move].COLOR_FROM == LIGHT) {
                //System.out.println("Debug: undo hite ep ");

                //Put the white pawn back
                brd.piece[History[undo_move].FROM] = 0;
                brd.color[History[undo_move].FROM] = 0;

                //Put the  black pawn back
                brd.piece[History[undo_move].TO + 8] = 0;
                brd.color[History[undo_move].TO + 8] = 1;

                //Clear ep square
                brd.piece[History[undo_move].TO] = 6;
                brd.color[History[undo_move].TO] = 6;

            } else {
                //System.out.println("Debug: undo lack ep ");

                //Put the black pawn back
                brd.piece[History[undo_move].FROM] = 0;
                brd.color[History[undo_move].FROM] = 1;

                //Put the white pawn back
                brd.piece[History[undo_move].TO - 8] = 0;
                brd.color[History[undo_move].TO - 8] = 0;

                //Clear ep square
                brd.piece[History[undo_move].TO] = 6;
                brd.color[History[undo_move].TO] = 6;

            }

            // update castle rigths
            castle = History[undo_move].CASTLE;
            // Update ep rights
            ep_to = History[undo_move].EP;

            History[undo_move].MOVE = null;
            move_index--;

        } else {

        }

    }

//******************************************************
//*         -=     End   of undo    =-                 *
//******************************************************
//******************************************************
//*  Type : public boolean                             *
//*  Args : int                                        *
//*  Aim  : Finds the king .                           *
//******************************************************
//*         -=     Start of find_king     =-           *
//******************************************************
    public boolean find_king(int temp_turn, int temp_oturn) {
        int king_index;

        for (int i = 0; i < 64; i++) {
            if (brd.piece[i] == K & brd.color[i] == temp_turn) {
                return roya(i, temp_oturn);
            }
        }

        return true;

    }

//******************************************************
//*         -=     End   of find_king   =-             *
//******************************************************
//******************************************************
//*  Type : public boolean                             *
//*  Args : int int                                    *
//*  Aim  : Seeks if the king  is in check.            *
//******************************************************
//*         -=     Start of roya     =-                *
//******************************************************
    public boolean roya(int king_index, int temp_turn) {

        for (int i = 0; i < 64; i++) {
            if (brd.color[i] == temp_turn) { //Check only for the opponent pieces

                if (brd.piece[i] == P) { //Check for Pawn Attack

                    if (temp_turn == LIGHT) { // White pawns
                        if (brd.col[i] != 0 && i - 9 == king_index) { // Eat left
                            return true;
                        }
                        if (brd.col[i] != 7 && i - 7 == king_index) { // Eat Right
                            return true;
                        }
                    } else { //Black Pawns
                        if (brd.col[i] != 7 && i + 9 == king_index) { // Eat left
                            return true;
                        }
                        if (brd.col[i] != 0 && i + 7 == king_index) { // Eat  Right
                            return true;
                        }
                    }
                } else { //Check for other pieces attack

                    for (int j = 0; j < direction[brd.piece[i]]; ++j) {
                        for (int n = i;;) {
                            n = brd.board_frame[brd.board_main[n]
                                    + brd.piece_move[brd.piece[i]][j]];
                            if (n == -1) {
                                break;
                            }
                            if (n == king_index) {
                                return true;
                            }
                            if (brd.color[n] != EMPTY) {
                                break;
                            }
                            if (!brd.slide[brd.piece[i]]) {
                                break;
                            }

                        }
                    }

                }

            }

        }
        return false;

    }

//******************************************************
//*         -=     End   of roya   =-                  *
//******************************************************
//******************************************************
//*  Type : public void                                *
//*  Args :  int,int,String,int                        *
//*  Aim  : Makes the move and stores any needed info. *
//******************************************************
//*         -=     Start of make_move     =-           *
//******************************************************
    public void make_move(int from, int to, String move, int move_type) throws
            IOException {
        //debug("make_move:: " + move + "[" + move_type + "]");
        //Check if the move is: castle
        //White is castling
        String mv;
        if (brd.piece[brd.board_frame[from]] == 5
                && (brd.board_frame[from] == 60 && brd.board_frame[to] == 62)) {
            mv = "o-o";
            CASTLE.make_castle(60, 62, 63, 61, 0, mv, 2);
            return;
        } else if (brd.piece[brd.board_frame[from]] == 5
                && (brd.board_frame[from] == 60
                && brd.board_frame[to] == 58)) {
            mv = "o-o-o";
            CASTLE.make_castle(60, 58, 56, 59, 0, mv, 3);
            return;
        }

        //Black is castling
        if (brd.piece[brd.board_frame[from]] == 5
                && (brd.board_frame[from] == 4 && brd.board_frame[to] == 6)) {
            mv = "o-o";
            CASTLE.make_castle(4, 6, 7, 5, 1, mv, 4);
            return;
        } else if (brd.piece[brd.board_frame[from]] == 5
                && (brd.board_frame[from] == 4 && brd.board_frame[to] == 2)) {
            mv = "o-o-o";
            CASTLE.make_castle(4, 2, 0, 3, 1, mv, 5);
            return;
        }

        //Check if the move is: castle
        //Check if the move is: en pasasnt
        if (move_type != 0 && move_type != 24) {

            if (move_type == 21 && ep_to != -1) { //En passant
                if (brd.color[brd.board_frame[from]] == LIGHT) {
                    EP.make_ep(0, brd.board_frame[from], brd.board_frame[to],
                            move, 21);
                } else {
                    EP.make_ep(1, brd.board_frame[from], brd.board_frame[to],
                            move, 21);
                }
                return;
            }
        }
        //Check if the move is: en pasasnt

        //Check if the move is: promote
        if (brd.piece[brd.board_frame[from]] == 0
                && brd.row[brd.board_frame[from]] == 6
                && brd.board_frame[to] <= 7) {
            String cmd;
            String f = brd.board_squares[from];
            String t = brd.board_squares[to];
            mv = "q";

            if (move_type != 0) {
                if (move_type == 6) { //Promote to Queen
                    mv = f + t + "q";
                    PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                            0, mv, 6);
                } else if (move_type == 7) { //Promote to Rook
                    mv = f + t + "r";
                    PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                            0, mv, 7);
                } else if (move_type == 8) { //Promote to Knight
                    mv = f + t + "n";
                    PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                            0, mv, 8);
                } else if (move_type == 9) { //Promote to Bishop
                    mv = f + t + "b";
                    PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                            0, mv, 9);
                }
            } else if ((move.charAt(4) == 'Q' || move.charAt(4) == 'q')) { //Promote to Queen
                mv = f + t + "q";
                PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                        0, mv, 6);
            } else if ((move.charAt(4) == 'R' || move.charAt(4) == 'r')) { //Promote to Rook
                mv = f + t + "r";
                PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                        0, mv, 7);
            } else if ((move.charAt(4) == 'N' || move.charAt(4) == 'n')) { //Promote to Knight
                mv = f + t + "n";
                PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                        0, mv, 8);
            } else if ((move.charAt(4) == 'B' || move.charAt(4) == 'b')) { //Promote to Bishop
                mv = f + t + "b";
                PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                        0, mv, 9);
            }

            return;

        }
        if (brd.piece[brd.board_frame[from]] == 0
                && brd.row[brd.board_frame[from]] == 1
                && brd.board_frame[to] >= 56) {

            String f = brd.board_squares[from];
            String t = brd.board_squares[to];
            mv = "q";

            if (move_type != 0) {
                if (move_type == 6) { //Promote to Queen
                    mv = f + t + "q";

                    PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                            1, mv, 6);
                } else if (move_type == 7) { //Promote to Rook
                    mv = f + t + "r";

                    PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                            1, mv, 7);
                } else if (move_type == 8) { //Promote to Knight
                    mv = f + t + "n";

                    PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                            1, mv, 8);
                } else if (move_type == 9) { //Promote to Bishop
                    mv = f + t + "b";

                    PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                            1, mv, 9);
                }
            } else if ((move.charAt(4) == 'Q' || move.charAt(4) == 'q')) { //Promote to Queen
                mv = f + t + "q";
                PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                        1, mv, 6);
            } else if ((move.charAt(4) == 'R' || move.charAt(4) == 'r')) { //Promote to Rook
                mv = f + t + "r";
                PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                        1, mv, 7);
            } else if ((move.charAt(4) == 'N' || move.charAt(4) == 'n')) { //Promote to Knight
                mv = f + t + "n";
                PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                        1, mv, 8);
            } else if ((move.charAt(4) == 'B' || move.charAt(4) == 'b')) { //Promote to Bishop
                mv = f + t + "b";
                PROMOTE.promote(brd.board_frame[from], brd.board_frame[to],
                        1, mv, 9);
            }

            return;

        }
        //Check if the move is: promote

        int temp_piece, temp_color, temp_piece_a, temp_color_a;

        //Store the old values
        temp_piece = brd.piece[brd.board_frame[from]];
        temp_color = brd.color[brd.board_frame[from]];
        temp_piece_a = brd.piece[brd.board_frame[to]];
        temp_color_a = brd.color[brd.board_frame[to]];

        //zero
        brd.piece[brd.board_frame[from]] = 6;
        brd.color[brd.board_frame[from]] = 6;

        //Write the values to the new square
        brd.piece[brd.board_frame[to]] = temp_piece;
        brd.color[brd.board_frame[to]] = temp_color;

        //Backup for the undo method
        History[move_index] = new move(from, to, temp_piece, temp_piece_a,
                temp_color, temp_color_a,
                move_type, move, castle, ep_to,
                FIFTY_RULE);

        move_index++;

        //Update the castle and en passant rights
        //...Castle
        castle &= CASTLE.castle_right[brd.board_frame[from]]
                & CASTLE.castle_right[brd.board_frame[to]];

        //...En passant
        if ((move_type & 8) > 0) { // Here is a nice trick.When a pawn advances two squares from
            //System.out.println("Pseudo_Legal : move_type" + move_type);
            if (temp_color == LIGHT) { // his starting position we send a move_type equal to 24
                ep_to = brd.board_frame[to] + 8; // so after the logical AND we get 24&8=1 ,which is >0 . ;-)
                //System.out.println("Pseudo_Legal :white  pawn advance" + to);
            } else {
                ep_to = brd.board_frame[to] - 8;
                //System.out.println("Pseudo_Legal : black pawn advance" + to);
            }

        } else {
            ep_to = -1;
        }

        eval_ = false;

    }

//******************************************************
//*         -=       End of make_move    =-            *
//******************************************************
//******************************************************
//*  Type : public boolean                             *
//*  Args :                                            *
//*  Aim  : Handles the repetition draw.               *
//******************************************************
//*    -=     Start of repetition_rule    =-           *
//******************************************************
    public boolean repetition_rule() throws IOException {

        byte occur = 1;
        //See if the last position has occured 2 times before
        if (move_index > 2) {
            for (int i = move_index - 2; i > 0; i--) {

                if (repeat_board[i] == repeat_board[move_index - 1]) {
                    occur++;
                }
            }
        }

        if (occur > 2) {
            return true;
        }
        return false;
    }

//******************************************************
//*        -=   End of repetition_rule   =-            *
//******************************************************
//******************************************************
//*  Type : public void                                *
//*  Args :  int,int                                   *
//*  Aim  : Handles the 50-moves draw.                 *
//******************************************************
//*         -=     Start of fifty_rule    =-           *
//******************************************************
    public void fifty_rule(int from, int to) throws IOException {

        //If a pawn is moved zero the FIFTY_RULE variable
        //If a capture is done   zero the FIFTY_RULE variable
        if (brd.piece[brd.board_frame[from]] == 0
                || brd.piece[brd.board_frame[to]] != 6) {
            FIFTY_RULE = 0;

            //Else add one more ply-move to the the FIFTY_RULE variable
        } else {
            FIFTY_RULE++;
        }

    }
//******************************************************
//*         -=       End of fifty_rule   =-            *
//******************************************************

    public void set_pondering(int value) {
        this.pondering = value;
    }

    public void set_random(int value) {
        this.random = value;
    }
    
//******************************************************
//*  Type : method                                     *
//*  Args : message		the string to write    *
//*  Aim  : writes a line to standard output           *                  
//******************************************************
//*         -=       Start of debug    =-              *
//******************************************************    
    public static void debug(String message) {
        System.out.println("# " + message );
        System.out.flush();
    }
//******************************************************
//*         -=        End of debug     =-              *
//******************************************************      
}
