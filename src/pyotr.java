
import java.io.*;
import java.lang.System;
import java.util.*;

import jMove4Chess.JMove;
import jBoard4Chess.JBoard;
import jPGNParser.JPGN;
import jXBoard4Chess.JXboard;

public class pyotr {

    public JMove moves;
    public JBoard jboard;
    public JXboard protocol;
    
    //Create some global operands and objects
    generator gen;          //Instance of the generator class

    BufferedReader keyb;    //For the keyboard input
    String cmd;             //Stores the command

    int program_turn = 6;  //Help the program to understand if is his turn or not
    //Default value is : 6

    final static String EDITION = "Club";
    final static String VERSION = "0.6";
    final static String YEAR = "2001-2016";    
    final static String BUILD = "28112016";

//******************************************************
//*  Type : Method                                     *
//*  Args : String[]                                   *
//*  Aim  : Decides in which mode to run Pyotr         *
//******************************************************
//*         -=     Start of main         =-            *
//******************************************************
    public static void main(String args[]) throws IOException {
        // Check if a parameter has been send with the command line
        if (args.length >= 1) {
            // If so start Pyotr in xboard mode
            new pyotr("x");
        } else {
            // If NOT start Pyotr in shell mode
            new pyotr();
        }
    }
//******************************************************
//*         -=       End of main         =-            *
//******************************************************

//******************************************************
//*  Type : public  void                               *
//*  Args :                                            *
//*  Aim  : Handles the xboard mode                    *
//******************************************************
//*         -=     Start of xboard       =-            *
//******************************************************
    public void xboard() throws IOException {

        protocol.send_features();

        init_xboard();

        //Please make your move...
        for (;;) {

            try {
                String[] cmd_info = protocol.identify(keyb.readLine());
                cmd = cmd_info[1];

                if (cmd.equals("xboard")) {
                    continue;
                }

                if (cmd.equals("protover")) {  //Set the protocol version
                    protocol.set_protocol_version(cmd_info[3]);
                    continue;
                }

                if (cmd.equals("accepted")) {  //Set the protocol version
                    protocol.set_accepted_feature(cmd_info[3]);
                    continue;
                }

                if (cmd.equals("rejected")) {  //Set the protocol version
                    protocol.set_rejected_feature(cmd_info[3]);
                    continue;
                }

                if (cmd.equals("random")) {
                    gen.set_random(1);
                    continue;
                }

                if (cmd.equals("hard")) {
                    gen.set_pondering(1);
                    continue;
                }

                if (cmd.equals("easy")) {
                    gen.set_pondering(0);
                    continue;
                }

                if (cmd.equals("new")) {
                    init_xboard();
                    program_turn = 1;
                    gen.set_random(0);
                    continue;
                }

                if (cmd.equals("quit")) {
                    System.exit(-1);
                }

                if (cmd.equals("force")) {
                    program_turn = 6;
                    continue;
                }

                if (cmd.equals("white")) { //White Plays and engine plays black
                    gen.TURN = jboard.BLACK_TO_PLAY;
                    gen.OTURN = jboard.WHITE_TO_PLAY;

                    program_turn = jboard.WHITE_TO_PLAY;

                    continue;
                }

                if (cmd.equals("black")) {  //Black plays and engine plays white
                    gen.TURN = jboard.WHITE_TO_PLAY;
                    gen.OTURN = jboard.BLACK_TO_PLAY;

                    program_turn = jboard.BLACK_TO_PLAY;

                    continue;
                }

                if (cmd.equals("time")) {  //Set the available time

//                    int availableTime = Integer.parseInt((String) cmd.substring(5));
                    int availableTime = Integer.parseInt((String) cmd_info[3]);

                    //Turn availableTime to seconds
                    availableTime /= 100;

                    //Set the maximum time for the move at the generator class
                    gen.maxTime = availableTime;
                    write("# Time left:" + gen.maxTime + " seconds");
                    continue;

                }

                if (cmd.equals("otim")) {  //Set the available time

//                    int availableTime = Integer.parseInt((String) cmd.substring(5));
                    int availableTime = Integer.parseInt((String) cmd_info[3]);

                    //Turn availableTime to seconds
                    availableTime /= 100;

                    //Set the maximum time for the move at the generator class
                    gen.maxoTime = availableTime;
                    write("# Time Opponent left:" + gen.maxoTime + " seconds");
                    continue;

                }

                if (cmd.equals("level")) {  //Set the time control mode
                    String[] data = ((String) cmd_info[3]).split(",");
                    int moves = Integer.parseInt((String) data[0]);
                    int base = Integer.parseInt((String) data[1]);
                    int inc = Integer.parseInt((String) data[2]);

                    //Time control is set to conventional chess clocks: all moves/Xminutes
                    if (moves == 0) {
                        write("# Time Control: all moves/Xminutes");
                        gen.TimeControl_AllMoves = true;
                        gen.TimeControl_XMoves = false;
                    } else {
                        write("# Time Control: Xmoves/Xminutes");
                        write("# Moves :" + moves + " in " + base + " minutes");
                        gen.TimeControl_AllMoves = false;
                        gen.TimeControl_XMoves = true;
                        gen.TimeControl_XMoves_Number = moves;
                    }

                    continue;
                }

                if (cmd.equals("post")) {
                    gen.POST = true;
                    continue;
                }

                if (cmd.equals("nopost")) {
                    gen.POST = false;
                    continue;
                }

                if (cmd.equals("edit")) {
                    //Tell Pyotr not to look at the opening book
                    gen.FEN = true;
                    gen.castle = 0;

                    int color = 6, piece = 6;

                    //Cleanup the board
                    for (int j = 0; j < 64; j++) {
                        gen.brd.piece[j] = 6;
                        gen.brd.color[j] = 6;
                    }
                    write("# Setup a FEN position:");
                    for (;;) {
                        cmd = keyb.readLine();
                        if (cmd.equals("#")) {
                            write("# Setup White:");
                            color = 0;
                            continue;
                        }
                        if (cmd.equals("c")) {
                            write("# Setup Black:");
                            color = 1;
                            continue;
                        }

                        if (cmd.equals(".")) {
                            write("# End of Setup:");
                            break;
                        } else {
                            //Find the piece

                            write("# piece : " + cmd.charAt(0));

                            if (cmd.charAt(0) == 'P') {
                                piece = 0;
                            } else if (cmd.charAt(0) == 'N') {
                                piece = 1;
                            } else if (cmd.charAt(0) == 'B') {
                                piece = 2;
                            } else if (cmd.charAt(0) == 'R') {
                                piece = 3;
                            } else if (cmd.charAt(0) == 'Q') {
                                piece = 4;
                            } else if (cmd.charAt(0) == 'K') {
                                piece = 5;
                            }

                            //Find the square
                            int square = gen.brd.board_frame[moves.getMoveSetupSquareInt(cmd)];
                            write("# Square : " + square);
                            gen.brd.piece[square] = piece;
                            gen.brd.color[square] = color;
                        }

                    }
                    gen.board_print();
                    continue;
                }

                if (cmd.equals("go")) {
                    program_turn = jboard.getSideToPlay();
                }

                if (program_turn == jboard.getSideToPlay()) {

                    int temp_side;
                    gen.brain();
                    //First check if a mate,stalemate flag is risen
                    if (gen.flagI_am_Mated_B || gen.flagI_am_Mated_W || gen.flagStalemated) {

                    } else {
                        write("move " + gen.History[gen.move_index - 1].MOVE);

                        //Add the position to repeat_board array
                        long hashKey = 0;

                        for (int p = 0; p < 64; p++) {
                            hashKey ^= gen.zobrist[jboard.getSideToPlay()][gen.brd.piece[p]][p];
                        }
                        //hashKey ^=gen.zobrist[SIDE][ gen.brd.piece[p] ][p];

                        gen.repeat_board[gen.move_index] = hashKey;

                        //Add the position to repeat_board array
                    }

                    // change the side to make a move
                    jboard.setOtherSideToPlay();
                    continue;
                } else if (parse_move(cmd)) {
                    // change the side to make a move
                    jboard.setOtherSideToPlay();
                    if (program_turn == jboard.getSideToPlay()) {

                        gen.brain();

                        if (gen.flagI_am_Mated_B || gen.flagI_am_Mated_W || gen.flagStalemated) {

                        } else {
                            write("move " + gen.History[gen.move_index - 1].MOVE);
//				 gen.board_print();
                        }

                        //---- Add the position to repeat_board array
                        long hashKey = 0;

                        for (int p = 0; p < 64; p++) {
                            hashKey ^= gen.zobrist[jboard.getSideToPlay()][gen.brd.piece[p]][p];
                        }

                        gen.repeat_board[gen.move_index] = hashKey;

                        //<--- Add the position to repeat_board array
                        
                        // change the side to make a move
                        jboard.setOtherSideToPlay();
                    }
                    continue;
                } else {

                    error("unknown command", cmd);

                }

            } catch (IOException ex) {
            }

        }

    }
//******************************************************
//*         -=       End of xboard       =-            *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Initialise the board of  a new game.       *
//******************************************************
//*         -=     Start of init_xboard       =-       *
//******************************************************
    public void init_xboard() throws IOException {
        // Reset All Board info
        jboard.reset();

        gen = new generator();

        gen.TURN    = jboard.WHITE_TO_PLAY;
        gen.OTURN   = jboard.BLACK_TO_PLAY;

    }
//******************************************************
//*         -=       End of init_xboard       =-       *
//******************************************************

//******************************************************
//*  Type : Constructor                                *
//*  Args :                                            *
//*  Aim  : Start up the programm by invoking the start*
//*         method.Prints some info     			   *
//*         about the name and the version of the      *
//*         programm. It also creates a BufferedReader *
//*         for the keyboard input.                    *
//******************************************************
//*         -=     Start of pyotr       =-         *
//******************************************************
    pyotr() throws IOException {
        Date today = new Date();
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(today);

        TimeZone tz = cal.getTimeZone();

        write("");
        write("Date :  " + cal.get(Calendar.DATE) + "-" + cal.get(Calendar.MONTH) + "-" + cal.get(Calendar.YEAR));
        write("Time :  " + cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND) + "   (TimeZone : " + tz.getID() + " )");

        //Starting the ascii mode
        write("");
        write("	   Pyotr " + EDITION + " " + VERSION + " " + YEAR);
        write("");
        write("	   +---------------------------------+");
        write("	   - Author : Marountas John         -");
        write("	   - Country: Greece                 -");
        write("	   - Email  : pyotr@digichess.gr     -");
        write("	   - URL    : www.digichess.gr/pyotr -");
        write("	   +---------------------------------+");
        write("");
        write(" Type 'help' for help! or start a new game with 'new' . ");
        write("");
        //Starting the ascii mode
        keyb = new BufferedReader(new InputStreamReader(System.in)); //Create the keyboard input
        // Initialize the moves class in order to parse and validate user's moves
        moves = new JMove();
        jboard = new JBoard();
        JPGN jpgn = new JPGN("book.pgn");
        jpgn.read();
        start();                                                      //Start the programm loop
    }
//******************************************************
//*         -=        End of pyotr       =-        *
//******************************************************

//******************************************************
//*  Type : Constructor                                *
//*  Args : String                                     *
//*  Aim  : Start up the programm in xboard mode.      *
//******************************************************
//*         -=     Start of pyotr       =-         *
//******************************************************
    pyotr(String x) throws IOException {
        keyb = new BufferedReader(new InputStreamReader(System.in)); //Create the keyboard input
        // Initialize the moves class in order to parse and validate user's moves
        moves = new JMove();
        jboard = new JBoard();
        String[] features = {
            "myname=\"Pyotr (" + EDITION + ") " + VERSION + " \"",
            "variants=\"normal\"",
            "ping=0",
            "setboard=0",
            "playother=0",
            "time=1",
            "sigint=0",
            "sigterm=0",
            "analyze=0",
            "debug=1",
            "done=1"
        };
        protocol = new JXboard(features);
        xboard();                                                      //Start the programm loop
    }
//******************************************************
//*         -=        End of pyotr       =-        *
//******************************************************

//******************************************************
//*  Type : boolean                                    *
//*  Args : String                                     *
//*  Aim  : Converts the move from algebric to integer *
//*         ,makes the move and forces the program  to *
//*         start thinking by invoking the brain method*
//******************************************************
//*         -=       Start of parse_move       =-      *
//******************************************************
    boolean parse_move(String move) throws IOException {

        if (!moves.parse(move)) {
            debug("parse_move:: invalid move: " + move);
            return false;
        } else {
            try {
                //From square
                int from = moves.getMoveFromSquareInt(move);
                //To square
                int to = moves.getMoveToSquareInt(move);

                gen.fifty_rule(from, to);
                //Finding the type of the move
                int move_type = 1;

                if (gen.brd.piece[gen.brd.board_frame[from]] == gen.P) {    //Create the Pawns moves
                    if (gen.brd.board_frame[to] == gen.ep_to) {
                        move_type = 21;
                    }
                    //For white
                    if (gen.brd.row[gen.brd.board_frame[from]] == 1 && gen.brd.color[gen.brd.board_frame[from] - 16] == gen.EMPTY) {
                        move_type = 24;   //Two forward
                    }              //For Black
                    if (gen.brd.row[gen.brd.board_frame[from]] == 6 && gen.brd.color[gen.brd.board_frame[from] + 16] == gen.EMPTY) {
                        move_type = 24;   //Two forward
                    }
                    if (move.length() == 5) {
                        if ((move.charAt(4) == 'Q' || move.charAt(4) == 'q')) {             //Promote to Queen
                            move_type = 6;
                        } else if ((move.charAt(4) == 'R' || move.charAt(4) == 'r')) {             //Promote to Rook
                            move_type = 7;
                        } else if ((move.charAt(4) == 'N' || move.charAt(4) == 'n')) {            //Promote to Knight
                            move_type = 8;
                        } else if ((move.charAt(4) == 'B' || move.charAt(4) == 'b')) {            //Promote to Bishop
                            move_type = 9;
                        }
                    }

                }
                //Finding the type of the move

                gen.make_move(from, to, move, move_type);

                //Add the position to repeat_board array
                long hashKey = 0;

                for (int p = 0; p < 64; p++) {
                    hashKey ^= gen.zobrist[gen.TURN][gen.brd.piece[p]][p];
                }

                gen.repeat_board[gen.move_index] = hashKey;

                //Add the position to repeat_board array
                return true;

            } catch (NumberFormatException ex) {

                return false;
                //Illegal move

            }

        }

    }
//******************************************************
//*         -=        End of parse_move      =-        *
//******************************************************

//******************************************************
//*  Type : method                                     *
//*  Args :                                            *
//*  Aim  : This method is used for the infinite loop  *
//*         of the programm .There is an If statement  *
//*         which controls if the user input is command*
//*         or a new move.                             *
//******************************************************
//*         -=       Start of start       =-           *
//******************************************************
    void start() throws IOException {
        write(" Pyotr> ");
        for (;;) {

            try {

                cmd = keyb.readLine();
                //DEBUG
                jboard.debug();

                if (cmd.equals("new")) {
                    write(" Pyotr> Starting a new game... ");
                    new_game();
                    if (jboard.getSideToPlay() == jboard.WHITE_TO_PLAY) {
                        continue;
                    }

                }
                if (cmd.equals("board")) {
                    if (!jboard.getGameStarted()) {
                        write(" Pyotr> You haven't started a game yet ! ");
                        write(" Pyotr>");
                    } else {
                        write(" Pyotr> Current position ");
                        gen.board_print();
                        write(" Pyotr>");
                    }
                    continue;
                }

                if (cmd.equals("history") || cmd.equals("hist")) {
                    if (!jboard.getGameStarted()) {
                        write(" Pyotr> You haven't started a game yet ! ");
                        write(" Pyotr>");
                    } else {
                        write(" Pyotr> History: ");
                        gen.history_print();
                        write("");
                        write(" Pyotr>");
                    }
                    continue;
                }

                if (cmd.equals("all")) {
                    if (!jboard.getGameStarted()) {
                        write(" Pyotr> You haven't started a game yet ! ");
                        write(" Pyotr>");
                    } else {
                        write(" Pyotr> \nPossible moves: ");
                        gen.movelist_print();
                        write(" Pyotr>");
                    }
                    continue;
                }

                if (cmd.equals("undo")) {
                    if (!jboard.getGameStarted()) {
                        write(" Pyotr> You haven't started a game yet ! ");
                        write(" Pyotr>");
                    } else {
                        write(" Pyotr> Undo last move: ");
                        write(" Pyotr>");
                        gen.undo(gen.move_index - 1);
                    }
                    continue;
                }

                if (cmd.equals("help")) {
                    write(" Pyotr> <><><><><><><>< Help ><><><><><><><><><>");
                    write(" Pyotr> <>  new     - Start a new game.       <>");
                    write(" Pyotr> <>  history - Print movelist.         <>");
                    write(" Pyotr> <>  hist    - Same as history.        <>");
                    write(" Pyotr> <>  all     - All possible moves.     <>");
                    write(" Pyotr> <>  undo    - Undo last move.         <>");
                    write(" Pyotr> <>  board   - Print position.         <>");
                    write(" Pyotr> <>  help    - This help screen.       <>");
                    write(" Pyotr> <>  go      - Force program to play.  <>");
                    write(" Pyotr> <>  white   - Change turn to white.   <>");
                    write(" Pyotr> <>  black   - Change turn to black.   <>");
                    write(" Pyotr> <>  exit    - Exit pyotr.             <>");
                    write(" Pyotr> <>  quit    - Same as exit.           <>");
                    write(" Pyotr> <>  q       - Same as exit.           <>");
                    write(" Pyotr> <><><><><><><>< Help ><><><><><><><><><>");
                    write(" Pyotr>");
                    continue;
                }

                if (cmd.equals("exit") || cmd.equals("quit") || cmd.equals("q")) {
                    write(" Pyotr> Bye bye !! ");
                    System.exit(-1);

                }
                if (cmd.equals("white")) { //Change side to play to White
                    if (!jboard.getGameStarted()) {
                        write(" Pyotr> You haven't started a game yet ! ");
                        write(" Pyotr>");
                    } else {
                        /*
	    	 SIDE=1;
            OSIDE=0;

            gen.TURN  = 1;
            gen.OTURN = 0;

		    program_turn = 0;
                         */
                        jboard.setSideToPlay(jboard.WHITE_TO_PLAY);
                        gen.TURN = jboard.BLACK_TO_PLAY;
                        gen.OTURN = jboard.WHITE_TO_PLAY;

                        program_turn = jboard.getSideToPlay();

                    }

                    continue;
                }

                if (cmd.equals("black")) {  //Change side to play to Black
                    if (!jboard.getGameStarted()) {
                        write(" Pyotr> You haven't started a game yet ! ");
                        write(" Pyotr>");
                    } else {
                        /*
	    	 SIDE=0;
             OSIDE=1;

             gen.TURN  = 0;
             gen.OTURN = 1;

 		     program_turn = 1;
                         */
                        jboard.setSideToPlay(jboard.BLACK_TO_PLAY);
                        gen.TURN = jboard.WHITE_TO_PLAY;
                        gen.OTURN = jboard.BLACK_TO_PLAY;

                        program_turn = jboard.getSideToPlay();

                    }
                    continue;
                }

                if (cmd.equals("go")) {
                    if (!jboard.getGameStarted()) {
                        write(" Pyotr> You haven't started a game yet ! ");
                        write(" Pyotr>");
                        continue;
                    } else {
                        //program_turn = SIDE;
                        program_turn = jboard.getSideToPlay();
                    }

                }

                //if( SIDE == program_turn){
                if (program_turn == jboard.getSideToPlay()) {

                    int temp_side;
                    gen.brain();
                    if (gen.last == 0) {

                    } else {
                        write("move " + gen.History[gen.move_index - 1].MOVE);
                        write(" Pyotr>");
                        //Add the position to repeat_board array
                        long hashKey = 0;

                        for (int p = 0; p < 64; p++) {
                            hashKey ^= gen.zobrist[jboard.getSideToPlay()][gen.brd.piece[p]][p];
                        }

                        gen.repeat_board[gen.move_index] = hashKey;
                        //Add the position to repeat_board array

                    }


                    /*
		        temp_side = SIDE;
		        SIDE  = OSIDE;
		        OSIDE = temp_side;
                     */
                    continue;
                } else if (jboard.getGameStarted() && parse_move(cmd)) {
                    write(" Pyotr> Legal Move.Pyotr is thinking...   ");
                    /*
			 int temp_side;
		     temp_side = SIDE;
		     SIDE  = OSIDE;
		     OSIDE = temp_side;
                     */
                    jboard.setOtherSideToPlay();
                    //if( SIDE == program_turn){
                    if (program_turn == jboard.getSideToPlay()) {
                        gen.brain();
                        if (gen.last == 0) {

                        } else {
                            write("move " + gen.History[gen.move_index - 1].MOVE);
                        }

                        //Add the position to repeat_board array
                        long hashKey = 0;

                        for (int p = 0; p < 64; p++) {
                            hashKey ^= gen.zobrist[jboard.getSideToPlay()][gen.brd.piece[p]][p];
                        }

                        gen.repeat_board[gen.move_index] = hashKey;
                        //Add the position to repeat_board array
                        /*
		  temp_side = SIDE;
		  SIDE  = OSIDE;
		  OSIDE = temp_side;
                         */
                        jboard.setOtherSideToPlay();
                    }
                    write(" Pyotr>");
                    continue;
                } else {

                    write(" Pyotr> Wrong Move  or Unknown command. ");
                    write(" Pyotr>");
                }

            } catch (IOException ex) {
            }
        }
    }
//******************************************************
//*         -=        End of start      =-             *
//******************************************************

//******************************************************
//*  Type : method                                     *
//*  Args :                                            *
//*  Aim  : This method is used for setting up a new   *
//*         game. After the intialitation the method   *
//*         either forces the player to move either the*
//*         programm to calculate.                     *
//******************************************************
//*         -=       Start of new_game    =-           *
//******************************************************
    void new_game() throws IOException {
        // Initialize the game
        jboard.reset();
        jboard.setGameStarted(true);

        write(" Pyotr> Choose white[0] or black[1]:");
        gen = new generator();
        try {
            cmd = keyb.readLine();
            if (cmd.charAt(0) == '0') {
                write(" Pyotr> You have White");
                write(" Pyotr> Please make your move...");
                write(" Pyotr>");
                //SIDE=0;
                //OSIDE=1;            
                program_turn = jboard.BLACK_TO_PLAY;
                gen.TURN = jboard.WHITE_TO_PLAY;
                gen.OTURN = jboard.BLACK_TO_PLAY;

            } else if (cmd.charAt(0) == '1') {
                write(" Pyotr> You have Black");
                write(" Pyotr> Wait,the computer is thinking...");
                //SIDE=1;
                //OSIDE=0;
                //program_turn = SIDE;
                program_turn = jboard.WHITE_TO_PLAY;
                gen.TURN = jboard.BLACK_TO_PLAY;
                gen.OTURN = jboard.WHITE_TO_PLAY;
            }

        } catch (Exception ex) {
            write(" Pyotr> Error ! Please choose either white[0] or black[1]");
        }

        //gen.TURN  = SIDE;
        //gen.OTURN = OSIDE;
    }
//******************************************************
//*         -=        End of new_game     =-           *
//******************************************************

//******************************************************
//*  Type : method                                     *
//*  Args : message		the string to write    *
//*  Aim  : rites a line to standard output            *                  
//******************************************************
//*         -=       Start of write    =-              *
//******************************************************    
    public static void write(String message) {
        System.out.println(message);
        System.out.flush();
    }
//******************************************************
//*         -=        End of write     =-              *
//******************************************************      

//******************************************************
//*  Type : method                                     *
//*  Args : message		the error to write     *
//*  Aim  : rites a line to standard output            *                  
//******************************************************
//*         -=       Start of write    =-              *
//******************************************************    
    public static void error(String message, String command) {
        System.out.println("Error (" + message + "): " + command);
        System.out.flush();
    }
//******************************************************
//*         -=        End of write     =-              *
//******************************************************      

//******************************************************
//*  Type : method                                     *
//*  Args : message		the string to write    *
//*  Aim  : writes a line to standard output           *                  
//******************************************************
//*         -=       Start of debug    =-              *
//******************************************************    
    public static void debug(String message) {
        System.out.println("# " + message);
        System.out.flush();
    }
//******************************************************
//*         -=        End of debug     =-              *
//******************************************************     
}
