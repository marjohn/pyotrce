
import java.io.*;

public class evaluate {

    board brd;
    generator gen;

    int[] piece_value = { //the material value of each Piece
        1, 3, 3, 4, 9, 10
    };

//******************************************************
//*  Type : public                                     *
//*  Args :                                            *
//*  Aim  : The constructor of the class.              *
//******************************************************
//*         -=     Start of time              =-       *
//****************************************************** 
    public evaluate(board brd, generator gen) throws IOException {
        this.brd = brd;
        this.gen = gen;

    }
//******************************************************
//*         -=     End   of time              =-       *
//****************************************************** 

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : This method is used to evaluate all the    *
//*         moves and choose the best one ! Currently  *
//*         the program select a random one !         *
//******************************************************
//*         -=     Start of Evaluate         =-        *
//****************************************************** 
    public double Evaluate(int WHO_PLAY, int move_index) throws IOException {

        //=========
        int LIGHT = 0, DARK = 1;

//eval_=true;
        //Evaluate position
        double White_Total = 0.0, Black_Total = 0.0, balance = 0.0;
        for (int l = 0; l < 64; l++) {
            if (brd.color[l] == 0) {
                White_Total += piece_value[brd.piece[l]];

                if (brd.piece[l] == 0) {                 //Give White Pawn a bonus
                    White_Total += brd.wpawn_post[l];
                } else if (brd.piece[l] == 1) {                 //Give White knight a bonus
                    White_Total += brd.knight_post[l];
                } else if (brd.piece[l] == 2) {                 //Give White Bishop a bonus
                    White_Total += brd.bishop_post[l];
                } else if (brd.piece[l] == 3) {                 //Give White Rook a bonus
                    White_Total += brd.wrook_post[l];
                } else if (brd.piece[l] == 4) {                 //Give White Queen a bonus
                    White_Total += brd.queen_post[l];
                    if (move_index <= 8 && l != 59) {
                        White_Total -= 0.4;
                    }
                } else if (brd.piece[l] == 5) {                 //Give White king a bonus
                    White_Total += w_king_safety(l);             // Evaluate the safety of the white
                    // king

                    White_Total += brd.wking_post[l];
                    White_Total += 500;                          //Add a large bonus for the king
                    //This will help us when checking
                    // if a king has been captured so
                    // the previous move was illegal.

                }

            } else if (brd.color[l] == 1) {
                Black_Total += piece_value[brd.piece[l]];

                if (brd.piece[l] == 0) {                 //Give Black Pawn a bonus
                    Black_Total += brd.bpawn_post[l];
                } else if (brd.piece[l] == 1) {
                    Black_Total += brd.knight_post[l];       //Give Black knight a bonus
                } else if (brd.piece[l] == 2) {                 //Give Black Bishop a bonus
                    Black_Total += brd.bishop_post[l];
                } else if (brd.piece[l] == 3) {                 //Give Black Rook a bonus
                    Black_Total += brd.brook_post[l];
                } else if (brd.piece[l] == 4) {                 //Give Black Queen a bonus
                    Black_Total += brd.queen_post[l];
                    if (move_index <= 8 && l != 3) {
                        Black_Total -= 0.4;
                    }

                } else if (brd.piece[l] == 5) {                 //Give Black king a bonus
                    Black_Total += b_king_safety(l);             // Evaluate the safety of the Black
                    // king
                    Black_Total += brd.bking_post[l];
                    Black_Total += 500;                          //Add a large bonus for the king
                    //This will help us when checking
                    // if a king has been captured so
                    // the previous move was illegal.

                }

            }
        }

        if (WHO_PLAY == LIGHT) {
            balance = Black_Total - White_Total;
        } else {
            balance = White_Total - Black_Total;

        }

//  brd.movelist[ k  ][3] = balance;
        //Check if the side can check and give a bonus
        if (gen.find_king(1, 0) ? WHO_PLAY == LIGHT : gen.find_king(0, 1)) {
            if (WHO_PLAY == LIGHT) {

                balance += 0.9;
            } else {

                balance += 0.9;
            }
        }

        //Check if the side can mate
        return balance;

        //=========
    }
//******************************************************
//*         -=       End of Evaluate         =-        *
//****************************************************** 

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Evaluate the position of the black king.   *
//******************************************************
//*         -=     Start of b_king_safety    =-        *
//****************************************************** 
    double b_king_safety(int king_position) throws IOException {

        double b_king_bonus = 0.0;

        //Give a penalty if the pawns of the castle are missing
        //If Black has o-o
        if (king_position == 6) {
            //Extra bonus because Black castled
            b_king_bonus += 0.4;

            for (int i = 13; i < 16; i++) {
                if (brd.piece[i] == 0 && brd.color[i] == 1) {
                    b_king_bonus += 0.1;
                } else {
                    b_king_bonus -= 0.4;
                }

            }

        } else if (king_position == 2) {
            //If Black has o-o-o
            b_king_bonus += 0.05;

            for (int i = 9; i < 12; i++) {
                if (brd.piece[i] == 0 && brd.color[i] == 1) {
                    b_king_bonus += 0.1;
                } else {
                    b_king_bonus -= 0.3;
                }

            }

        } else {
            b_king_bonus -= 0.3;
        }

        return b_king_bonus;
    }
//******************************************************
//*         -=     End of b_king_safety    =-          *
//******************************************************

//******************************************************
//*  Type : public void                                *
//*  Args :                                            *
//*  Aim  : Evaluate the position of the white king.   *
//******************************************************
//*         -=     Start of w_king_safety    =-        *
//****************************************************** 
    double w_king_safety(int king_position) throws IOException {

        double w_king_bonus = 0.0;

        //Give a penalty if the pawns of the castle are missing
        //If Black has o-o
        if (king_position == 62) {
            //Extra bonus because Black castled
            w_king_bonus += 0.1;

            for (int i = 53; i < 56; i++) {
                if (brd.piece[i] == 0 && brd.color[i] == 0) {
                    w_king_bonus += 0.1;
                } else {
                    w_king_bonus -= 0.3;
                }

            }

        } else if (king_position == 58) {
            //If Black has o-o-o
            w_king_bonus += 0.05;

            for (int i = 49; i < 51; i++) {
                if (brd.piece[i] == 0 && brd.color[i] == 0) {
                    w_king_bonus += 0.1;
                } else {
                    w_king_bonus -= 0.3;
                }

            }

        } else {
            w_king_bonus -= 0.3;
        }

        return w_king_bonus;
    }
//******************************************************
//*         -=     End of w_king_safety    =-          *
//******************************************************

}
