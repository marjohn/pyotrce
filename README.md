# Pyotr Chess Engine #

This is the repository of the chess engine Pyotr.

Currently working on the Club Edition of the engine.

### About ###

Pyotr is a simple chess engine written in Java. You can play via the command line or by using the Winboard chess software.

### About the Author ###

John Marountas has studied Agricultural Engineering and is working since 2000 as a Developer.

His first contact with a programming language goes back to 1995, with Fortran 77. It was back then when the idea of the creation of a chess program was born.

His hobbies are chess, tennis and guitar !

Any feedback is welcome ! Please feel free to send me an email at : i.marountas@gmail.com

### History of the engine

You can learn more about the history of the engine at the [History](https://bitbucket.org/marjohn/pyotrce/wiki/History) page.

### Please help ! ###

Have a look at the [Frequently Asked Questions](https://bitbucket.org/marjohn/pyotrce/wiki/Frequently%20Asked%20Questions) page.
